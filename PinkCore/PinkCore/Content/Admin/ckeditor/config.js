/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	    config.toolbar = [
        //{ name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        //{ name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
        //{ name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
        //'/',
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', /*'Strike', 'Subscript', 'Superscript', */'-', 'RemoveFormat'] },
        { name: 'paragraph', groups: ['align', 'bidi'], items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Outdent', 'Indent', 'BulletedList', 'NumberedList'] },
        { name: 'links', items: ['Link', 'Unlink'/*, 'Anchor'*/] },
        { name: 'insert', items: ['Image', /*'Flash',*/ 'Table', 'HorizontalRule'/*, 'Smiley'*/, 'SpecialChar'/*, 'PageBreak'*/, 'Iframe', 'Youtube'] },
        //'/',
        { name: 'styles', items: [/*'Styles', 'Format', 'Font',*/ 'FontSize'] },
        { name: 'colors', items: ['TextColor', 'BGColor'] },
        //{ name: 'others', items: ['-'] },
        { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source'/*, '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'*/] },
        //{ name: 'tools', items: ['Maximize', 'ShowBlocks'] },
        //{ name: 'about', items: ['About'] }
    ];
};

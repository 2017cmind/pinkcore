﻿using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Areas.Admin.ViewModels.Product;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ProductAdminController : BaseAdminController
    {
        private ProductRepository productRepository = new ProductRepository();
        private TopicRepository topicRepository = new TopicRepository();
        // GET: Admin/ProductAdmin
        public ActionResult Index(ProductIndexView model)
        {
            var query = productRepository.Query(model.Name, (int)model.Type, (int)Language.zh_TW, model.Status);
            var pageResult = query.ToPageResult<RelProductProductTranslations>(model);
            model.PageResult = Mapper.Map<PageResult<ProductIndexItemView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new ProductView();
            if (id != 0)
            {
                model = productRepository.FindBy(id);
            }
            else
            {
                DateTime dtCurrentTime;
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
                dtCurrentTime = TimeZoneInfo.ConvertTime(DateTime.Now, tzi);
                model.OnlineTime = dtCurrentTime.ToString("yyyy/MM/dd");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductView model)
        {
            var imageHasFile = ImageHelper.CheckFileExists(model.ImageFile);
            var hoverImageHasFile = ImageHelper.CheckFileExists(model.HoverImageFile);

            model.SupportLan = model.supportLans != null ? string.Join(",", model.supportLans) : "";

            if (ModelState.IsValid)
            {
                Product data = Mapper.Map<Product>(model);

                var productTranslations = new List<ProductTranslations>();
                var productTranslationTw = Mapper.Map<ProductTranslations>(model.ProductTranslationViewTw);
                var productTranslationCn = Mapper.Map<ProductTranslations>(model.ProductTranslationViewCn);
                var productTranslationUs = Mapper.Map<ProductTranslations>(model.ProductTranslationViewUs);
                var productTranslationJp = Mapper.Map<ProductTranslations>(model.ProductTranslationViewJp);
                var productTranslationKr = Mapper.Map<ProductTranslations>(model.ProductTranslationViewKr);

                productTranslations.Add(productTranslationTw);
                productTranslations.Add(productTranslationCn);
                productTranslations.Add(productTranslationUs);
                productTranslations.Add(productTranslationJp);
                productTranslations.Add(productTranslationKr);

                var twImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewTw.ImageFile);
                var cnImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewCn.ImageFile);
                var usImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewUs.ImageFile);
                var jpImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewJp.ImageFile);
                var krImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewKr.ImageFile);


                if (twImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationTw.Image);
                    productTranslationTw.Image = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewTw.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagezh");
                }
                if (cnImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationCn.Image);
                    productTranslationCn.Image = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewCn.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagecn");
                }
                if (usImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationUs.Image);
                    productTranslationUs.Image = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewUs.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imageus");
                }
                if (jpImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationJp.Image);
                    productTranslationJp.Image = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewJp.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagejp");
                }
                if (krImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationKr.Image);
                    productTranslationKr.Image = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewKr.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagekr");
                }

                var twHoverImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewTw.HoverImageFile);
                var cnHoverImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewCn.HoverImageFile);
                var usHoverImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewUs.HoverImageFile);
                var jpHoverImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewJp.HoverImageFile);
                var krHoverImageHasFile = ImageHelper.CheckFileExists(model.ProductTranslationViewKr.HoverImageFile);

                if (twHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationTw.HoverImage);
                    productTranslationTw.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewTw.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagezh");
                }
                if (cnHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationCn.HoverImage);
                    productTranslationCn.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewCn.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagecn");
                }
                if (usHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationUs.HoverImage);
                    productTranslationUs.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewUs.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImageus");
                }
                if (jpHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationJp.HoverImage);
                    productTranslationJp.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewJp.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagejp");
                }
                if (krHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, productTranslationKr.HoverImage);
                    productTranslationKr.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.ProductTranslationViewKr.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagekr");
                }

                if (imageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.Image);
                    data.Image = ImageHelper.SaveImage(PhotoFolder, model.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "Image");
                }
                if (hoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.HoverImage);
                    data.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "HoverImage");
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;
                DateTime onlineTime = DateTime.ParseExact(model.OnlineTime, "yyyy/MM/dd", null);
                data.OnlineTime = onlineTime;

                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    model.ID = productRepository.Insert(data, productTranslations);
                }
                else
                {
                    productRepository.Update(data, productTranslations);
                }
                ShowMessage(true, "儲存成功");
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
          
            var query = topicRepository.Query("",id,null,(int)Language.zh_TW);
            if (query.Any()) {
                ShowMessage(false, "請先刪除有包含此遊戲的話題，才能刪除此遊戲");

            }
            else {
                DeleteImage(id, (int)DeviceType.PC);
                DeleteImage(id, (int)DeviceType.PCHover);

                var images = productRepository.DeleteImages(id);

                foreach (var item in images)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, item);
                }
                var hoverImages = productRepository.DeleteHoverImages(id);
                foreach (var item in hoverImages)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, item);
                }
                productRepository.Delete(id);
                ShowMessage(true, "刪除成功");
            }


            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, int type, int language = 0)
        {
            string image;
            if (language != 0)
            {
                image = productRepository.DeleteImage(id, type, language);
            }
            else
            {
                image = productRepository.DeleteImage(id, type);
            }
            ImageHelper.DeletePhoto(PhotoFolder, image);

            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }
    }
}
﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.AgeConfirm;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using PinkCore.Repositories;
using PinkCore.Models;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.ActionFilters;

namespace PinkCore.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class AgeConfirmAdminController : BaseAdminController
    {
        private AgeConfirmRepository ageConfirmRepository = new AgeConfirmRepository();

        // GET: Admin/AgeConfirmAdmin
        public ActionResult Edit()
        {
            var model = new AgeConfirmView();
            var dataCount = ageConfirmRepository.GetAll().Count();

            if (dataCount == 0)
            {
                AgeConfirm data = Mapper.Map<AgeConfirm>(model);

                var ageConfirmTranslations = new List<AgeConfirmTranslations>();
                var ageConfirmTranslationTw = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewTw);
                var ageConfirmTranslationCn = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewCn);
                var ageConfirmTranslationUs = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewUs);
                var ageConfirmTranslationJp = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewJp);
                var ageConfirmTranslationKr = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewKr);

                ageConfirmTranslations.Add(ageConfirmTranslationTw);
                ageConfirmTranslations.Add(ageConfirmTranslationCn);
                ageConfirmTranslations.Add(ageConfirmTranslationUs);
                ageConfirmTranslations.Add(ageConfirmTranslationJp);
                ageConfirmTranslations.Add(ageConfirmTranslationKr);

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;
                data.Creater = adminId;
                ageConfirmRepository.Insert(data, ageConfirmTranslations);
            }
            else {
                model = ageConfirmRepository.FirstData();
                model.ID = ageConfirmRepository.FirstData().ID;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AgeConfirmView model)
        {
            var pcImagehasFile = ImageHelper.CheckFileExists(model.PCImageFile);
            var moblieImagehasFile = ImageHelper.CheckFileExists(model.MoblieImageFile);

            if (ModelState.IsValid)
            {
                AgeConfirm data = Mapper.Map<AgeConfirm>(model);

                var ageConfirmTranslations = new List<AgeConfirmTranslations>();
                var ageConfirmTranslationTw = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewTw);
                var ageConfirmTranslationCn = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewCn);
                var ageConfirmTranslationUs = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewUs);
                var ageConfirmTranslationJp = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewJp);
                var ageConfirmTranslationKr = Mapper.Map<AgeConfirmTranslations>(model.AgeConfirmTranslationViewKr);

                ageConfirmTranslations.Add(ageConfirmTranslationTw);
                ageConfirmTranslations.Add(ageConfirmTranslationCn);
                ageConfirmTranslations.Add(ageConfirmTranslationUs);
                ageConfirmTranslations.Add(ageConfirmTranslationJp);
                ageConfirmTranslations.Add(ageConfirmTranslationKr);

                var twPCImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewTw.PCImageFile);
                var cnPCImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewCn.PCImageFile);
                var usPCImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewUs.PCImageFile);
                var jpPCImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewJp.PCImageFile);
                var krPCImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewKr.PCImageFile);

                var twMoblieImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewTw.MoblieImageFile);
                var cnMoblieImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewCn.MoblieImageFile);
                var usMoblieImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewUs.MoblieImageFile);
                var jpMoblieImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewJp.MoblieImageFile);
                var krMoblieImageHasFile = ImageHelper.CheckFileExists(model.AgeConfirmTranslationViewKr.MoblieImageFile);

                if (twPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationTw.PCImage);
                    ageConfirmTranslationTw.PCImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewTw.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff")+ "-PCImagezh");
                }
                if (cnPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationCn.PCImage);
                    ageConfirmTranslationCn.PCImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewCn.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagecn");
                }
                if (usPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationUs.PCImage);
                    ageConfirmTranslationUs.PCImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewUs.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImageus");
                }
                if (jpPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationJp.PCImage);
                    ageConfirmTranslationJp.PCImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewJp.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagejp");
                }
                if (krPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationKr.PCImage);
                    ageConfirmTranslationKr.PCImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewKr.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagekr");
                }

                if (twMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationTw.MoblieImage);
                    ageConfirmTranslationTw.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewTw.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagezh");
                }
                if (cnMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationCn.MoblieImage);
                    ageConfirmTranslationCn.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewCn.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagecn");
                }
                if (usMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationUs.MoblieImage);
                    ageConfirmTranslationUs.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewUs.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImageus");
                }
                if (jpMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationJp.MoblieImage);
                    ageConfirmTranslationJp.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewJp.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagejp");
                }
                if (krMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, ageConfirmTranslationKr.MoblieImage);
                    ageConfirmTranslationKr.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.AgeConfirmTranslationViewKr.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagekr");
                }

                if (pcImagehasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.PCImage);
                    data.PCImage = ImageHelper.SaveImage(PhotoFolder, model.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImage");
                }
                if (moblieImagehasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.MoblieImage);
                    data.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImage");
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;

                //if (model.ID == 0)
                //{
                //    data.Creater = adminId;
                //    model.ID = ageConfirmRepository.Insert(data, ageConfirmTranslations);
                //}
                //else
                //{
                    ageConfirmRepository.Update(data, ageConfirmTranslations);
                //}
                ShowMessage(true, "儲存成功");
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult DeleteImage(int id, int type, int language = 0)
        {
            string image;
            //type 1:PCImage , 2.Moblie       
            if (language != 0)
            {
                image = ageConfirmRepository.DeleteImage(id, type, language);
            }
            else
            {
                image = ageConfirmRepository.DeleteImage(id, type);
            }
            ImageHelper.DeletePhoto(PhotoFolder, image);

            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }
    }
}
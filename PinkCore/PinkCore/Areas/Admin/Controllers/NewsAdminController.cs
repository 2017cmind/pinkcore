﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Areas.Admin.ViewModels.News;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class NewsAdminController : BaseAdminController
    {  
        private NewsRepository newsRepository = new NewsRepository();

        // GET: Admin/NewsAdmin
        public ActionResult Index(NewsIndexView model)
        {
            var query = newsRepository.Query(model.Title, model.ProductId, (int)Language.zh_TW, model.Status);
            var pageResult = query.ToPageResult<RelNewsNewsTranslations>(model);
            model.PageResult = Mapper.Map<PageResult<NewsIndexItemView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new NewsView();
            if (id != 0)
            {
                model = newsRepository.FindBy(id);
                DateTime dtCurrentTime = Convert.ToDateTime(model.OnlineTime);
                model.OnlineTime = dtCurrentTime.ToString("yyyy/MM/dd HH:mm");
            }
            else
            {
                DateTime dtCurrentTime;
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
                dtCurrentTime = TimeZoneInfo.ConvertTime(DateTime.Now, tzi);
                model.OnlineTime = dtCurrentTime.ToString("yyyy/MM/dd HH:mm");
            }
            return View(model);
        }      

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(NewsView model, FormCollection collection)
        {
            var imgHasFile = ImageHelper.CheckFileExists(model.ImageFile);

            var tw_query = !string.IsNullOrEmpty(model.NewsTranslationViewTw.Title) ? newsRepository.FindIdByTitle(model.NewsTranslationViewTw.Title) : null;
            var jp_query = !string.IsNullOrEmpty(model.NewsTranslationViewJp.Title) ? newsRepository.FindIdByTitle(model.NewsTranslationViewJp.Title) : null;
            var kr_query = !string.IsNullOrEmpty(model.NewsTranslationViewKr.Title) ? newsRepository.FindIdByTitle(model.NewsTranslationViewKr.Title) : null;
            var cn_query = !string.IsNullOrEmpty(model.NewsTranslationViewCn.Title) ? newsRepository.FindIdByTitle(model.NewsTranslationViewCn.Title) : null;
            var us_query = !string.IsNullOrEmpty(model.NewsTranslationViewUs.Title) ? newsRepository.FindIdByTitle(model.NewsTranslationViewUs.Title) : null;

            bool isrepeat = false;
            if (tw_query != null || jp_query != null || kr_query != null || cn_query != null || us_query != null)
            {
                var tw_title = model.NewsTranslationViewTw.Title;
                model.NewsTranslationViewTw.Title = model.NewsTranslationViewCn.Title == model.NewsTranslationViewTw.Title ? model.NewsTranslationViewTw.Title + " 繁" : model.NewsTranslationViewTw.Title;
                model.NewsTranslationViewCn.Title = model.NewsTranslationViewCn.Title == tw_title ? model.NewsTranslationViewCn.Title + " 簡" : model.NewsTranslationViewCn.Title;

                if (model.NewsTranslationViewTw.ID != 0)
                {
                    isrepeat = (tw_query != null && tw_query?.ID != model.NewsTranslationViewTw.ID) ||
                            (jp_query != null && jp_query?.ID != model.NewsTranslationViewJp.ID) ||
                            (kr_query != null && kr_query?.ID != model.NewsTranslationViewKr.ID) ||
                            (cn_query != null && cn_query?.ID != model.NewsTranslationViewCn.ID) ||
                            (us_query != null && us_query?.ID != model.NewsTranslationViewUs.ID);
                }
                else
                {
                    isrepeat = tw_query != null || jp_query != null || kr_query != null || cn_query != null || us_query != null;
                }

                if (isrepeat)
                {
                    ShowMessage(false, "標題不能跟其他話題重複");
                }
            }
            if (ModelState.IsValid && !isrepeat)
            {
                News data = Mapper.Map<News>(model);

                var newsTranslations = new List<NewsTranslations>();
                var newsTranslationTw = Mapper.Map<NewsTranslations>(model.NewsTranslationViewTw);
                var newsTranslationCn = Mapper.Map<NewsTranslations>(model.NewsTranslationViewCn);
                var newsTranslationUs = Mapper.Map<NewsTranslations>(model.NewsTranslationViewUs);
                var newsTranslationJp = Mapper.Map<NewsTranslations>(model.NewsTranslationViewJp);
                var newsTranslationKr = Mapper.Map<NewsTranslations>(model.NewsTranslationViewKr);

                newsTranslations.Add(newsTranslationTw);
                newsTranslations.Add(newsTranslationCn);
                newsTranslations.Add(newsTranslationUs);
                newsTranslations.Add(newsTranslationJp);
                newsTranslations.Add(newsTranslationKr);
                string pattern = @"[^\u4e00-\u9fa50-9A-Za-z\uAC00-\uD7A3\s\u0800-\u4e00]";
                if (!string.IsNullOrEmpty(newsTranslationTw.Title))
                {
                    var tw_title = Regex.Replace(newsTranslationTw.Title, pattern, "");
                    newsTranslationTw.LinkUrl = Regex.Replace(tw_title, @"\s", "-");
                    newsTranslationTw.Title = newsTranslationTw.Title.Replace(" 繁", "");
                }
                if (!string.IsNullOrEmpty(newsTranslationCn.Title))
                {
                    var cn_title = Regex.Replace(newsTranslationCn.Title, pattern, "");
                    newsTranslationCn.LinkUrl = Regex.Replace(cn_title, @"\s", "-");
                    newsTranslationCn.Title = newsTranslationCn.Title.Replace(" 簡", "");
                }
                if (!string.IsNullOrEmpty(newsTranslationUs.Title))
                {
                    var us_title = Regex.Replace(newsTranslationUs.Title, pattern, "");
                    newsTranslationUs.LinkUrl = Regex.Replace(us_title, @"\s", "-");
                }
                if (!string.IsNullOrEmpty(newsTranslationJp.Title))
                {
                    var jp_title = Regex.Replace(newsTranslationJp.Title, pattern, "");
                    newsTranslationJp.LinkUrl = Regex.Replace(jp_title, @"\s", "-");
                }
                if (!string.IsNullOrEmpty(newsTranslationKr.Title))
                {
                    var kr_title = Regex.Replace(newsTranslationKr.Title, pattern, "");
                    newsTranslationKr.LinkUrl = Regex.Replace(kr_title, @"\s", "-");
                }
                var twImgHasFile = ImageHelper.CheckFileExists(model.NewsTranslationViewTw.ImageFile);
                var cnImgHasFile = ImageHelper.CheckFileExists(model.NewsTranslationViewCn.ImageFile);
                var usImgHasFile = ImageHelper.CheckFileExists(model.NewsTranslationViewUs.ImageFile);
                var jpImgHasFile = ImageHelper.CheckFileExists(model.NewsTranslationViewJp.ImageFile);
                var krImgHasFile = ImageHelper.CheckFileExists(model.NewsTranslationViewKr.ImageFile);

                if (twImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, newsTranslationTw.Image);
                    newsTranslationTw.Image = ImageHelper.SaveImage(PhotoFolder, model.NewsTranslationViewTw.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagezh");
                }
                if (cnImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, newsTranslationCn.Image);
                    newsTranslationCn.Image = ImageHelper.SaveImage(PhotoFolder, model.NewsTranslationViewCn.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagecn");
                }
                if (usImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, newsTranslationUs.Image);
                    newsTranslationUs.Image = ImageHelper.SaveImage(PhotoFolder, model.NewsTranslationViewUs.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imageus");
                }
                if (jpImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, newsTranslationJp.Image);
                    newsTranslationJp.Image = ImageHelper.SaveImage(PhotoFolder, model.NewsTranslationViewJp.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagejp");
                }
                if (krImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, newsTranslationKr.Image);
                    newsTranslationKr.Image = ImageHelper.SaveImage(PhotoFolder, model.NewsTranslationViewKr.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Imagekr");
                }

                if (imgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.Image);
                    data.Image = ImageHelper.SaveImage(PhotoFolder, model.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Image");
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;
                DateTime onlineTime = Convert.ToDateTime(model.OnlineTime);
                data.OnlineTime = onlineTime;
                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    model.ID = newsRepository.Insert(data, newsTranslations);
                }
                else
                {
                    newsRepository.Update(data, newsTranslations);
                }
                ShowMessage(true, "儲存成功");
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }
        public ActionResult Delete(int id)
        {
            var image = newsRepository.DeleteImage(id);
            ImageHelper.DeletePhoto(PhotoFolder, image);

            var images = newsRepository.DeleteImages(id);
            foreach (var item in images)
            {
                ImageHelper.DeletePhoto(PhotoFolder, item);
            }
           
            newsRepository.Delete(id);
            ShowMessage(true, "刪除成功");

            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, int type, int language = 0)
        {
            string image = "";
            //type 1:電腦圖  3.電腦版替換圖    
            if (language != 0)
            {
                image = newsRepository.DeleteImage(id, language);
            }
            else
            {
                image = newsRepository.DeleteImage(id);
            }
           
            ImageHelper.DeletePhoto(PhotoFolder, image);

            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }
    }
}
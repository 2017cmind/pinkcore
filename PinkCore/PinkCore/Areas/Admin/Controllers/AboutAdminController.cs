﻿using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Areas.Admin.ViewModels.About;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class AboutAdminController : BaseAdminController
    {
        private AboutRepository aboutRepository = new AboutRepository();

        // GET: Admin/AboutAdmin
        public ActionResult Edit()
        {
            var model = new AboutView();
            var dataCount = aboutRepository.GetAll().Count();
            if (dataCount == 0)
            {
                About data = Mapper.Map<About>(model);

                var aboutTranslations = new List<AboutTranslations>();
                var aboutTranslationTw = Mapper.Map<AboutTranslations>(model.AboutTranslationViewTw);
                var aboutTranslationCn = Mapper.Map<AboutTranslations>(model.AboutTranslationViewCn);
                var aboutTranslationUs = Mapper.Map<AboutTranslations>(model.AboutTranslationViewUs);
                var aboutTranslationJp = Mapper.Map<AboutTranslations>(model.AboutTranslationViewJp);
                var aboutTranslationKr = Mapper.Map<AboutTranslations>(model.AboutTranslationViewKr);

                aboutTranslations.Add(aboutTranslationTw);
                aboutTranslations.Add(aboutTranslationCn);
                aboutTranslations.Add(aboutTranslationUs);
                aboutTranslations.Add(aboutTranslationJp);
                aboutTranslations.Add(aboutTranslationKr);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;
                data.Creater = adminId;
                aboutRepository.Insert(data, aboutTranslations);
            }
            else
            {
                model = aboutRepository.FirstData();
                model.ID = aboutRepository.FirstData().ID;
                var tw = aboutRepository.GetImgs(model.AboutTranslationViewTw.ID, 0, 0, 0, 0);
                var jp = aboutRepository.GetImgs(0, 0, 0, 0, model.AboutTranslationViewJp.ID);
                var cn = aboutRepository.GetImgs(0, model.AboutTranslationViewCn.ID, 0, 0, 0);
                var us = aboutRepository.GetImgs(0, 0, model.AboutTranslationViewUs.ID, 0, 0);
                var kr = aboutRepository.GetImgs(0, 0, 0, model.AboutTranslationViewKr.ID, 0);

                model.AboutTranslationViewTw.AboutImgs = Mapper.Map<List<AboutImgView>>(tw);
                model.AboutTranslationViewCn.AboutImgs = Mapper.Map<List<AboutImgView>>(cn);
                model.AboutTranslationViewJp.AboutImgs = Mapper.Map<List<AboutImgView>>(jp);
                model.AboutTranslationViewKr.AboutImgs = Mapper.Map<List<AboutImgView>>(kr);
                model.AboutTranslationViewUs.AboutImgs = Mapper.Map<List<AboutImgView>>(us);

            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(AboutView model, FormCollection collection)
        {
            var aboutImgHasFile = ImageHelper.CheckFileExists(model.AboutImgFile);
            var contactImgHasFile = ImageHelper.CheckFileExists(model.ContactImgFile);
            var bannreImgHasFile = ImageHelper.CheckFileExists(model.BannerImgFile);
            var aboutImgszh_TWList = collection["AboutImgszh_TW.Index"] != null ? collection["AboutImgszh_TW.Index"].Split(',') : null;
            var aboutImgszh_CNList = collection["AboutImgszh_CN.Index"] != null ? collection["AboutImgszh_CN.Index"].Split(',') : null;
            var aboutImgsja_JPList = collection["AboutImgsja_JP.Index"] != null ? collection["AboutImgsja_JP.Index"].Split(',') : null;
            var aboutImgsko_KRList = collection["AboutImgsko_KR.Index"] != null ? collection["AboutImgsko_KR.Index"].Split(',') : null;
            var aboutImgsen_USList = collection["AboutImgsen_US.Index"] != null ? collection["AboutImgsen_US.Index"].Split(',') : null; ;

            if (ModelState.IsValid)
            {
                if (aboutImgszh_TWList != null) {
                    foreach (var item in aboutImgszh_TWList)
                    {
                        if (collection["AboutImgszh_TW[" + item + "].Status"] != null)
                        {
                            var status = collection["AboutImgszh_TW[" + item + "].Status"].Split(',')[0];
                            var des = collection["AboutImgszh_TW[" + item + "].Description"];
                            HttpPostedFileBase uploadFile = Request.Files["AboutImgszh_TW[" + item + "].ImageFile"];
                            var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                            var filename = "";
                            if (ImgHasFile)
                            {
                                ImageHelper.DeletePhoto(PhotoFolder, collection["AboutImgszh_TW[" + item + "].Image"]);
                                filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid() + "-Imagezh");
                            }
                            model.AboutTranslationViewTw.AboutImgs.Add(new AboutImgView()
                            {
                                Title = collection["AboutImgszh_TW[" + item + "].Title"],
                                Sort = Convert.ToInt32(collection["AboutImgszh_TW[" + item + "].Sort"]),
                                Status = Convert.ToBoolean(status),
                                Description = des,
                                AboutTranslationsId = model.AboutTranslationViewTw.ID,
                                Image = String.IsNullOrEmpty(filename) ? collection["AboutImgszh_TW[" + item + "].Image"] : filename,
                                Language = Language.zh_TW
                            });

                        }

                    }
                }

                if (aboutImgszh_CNList != null)
                {
                    foreach (var item in aboutImgszh_CNList)
                    {
                        if (collection["AboutImgszh_CN[" + item + "].Status"] != null)
                        {
                            var status = collection["AboutImgszh_CN[" + item + "].Status"].Split(',')[0];
                            var des = collection["AboutImgszh_CN[" + item + "].Description"];
                            HttpPostedFileBase uploadFile = Request.Files["AboutImgszh_CN[" + item + "].ImageFile"];
                            var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                            var filename = "";
                            if (ImgHasFile)
                            {
                                ImageHelper.DeletePhoto(PhotoFolder, collection["AboutImgszh_CN[" + item + "].Image"]);
                                filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid() + "-Imagecn");
                            }
                            model.AboutTranslationViewCn.AboutImgs.Add(new AboutImgView()
                            {
                                Title = collection["AboutImgszh_CN[" + item + "].Title"],
                                Sort = Convert.ToInt32(collection["AboutImgszh_CN[" + item + "].Sort"]),
                                Status = Convert.ToBoolean(status),
                                Description = des,
                                AboutTranslationsId = model.AboutTranslationViewCn.ID,
                                Image = String.IsNullOrEmpty(filename) ? collection["AboutImgszh_CN[" + item + "].Image"] : filename,
                                Language = Language.zh_CN
                            });
                        }
                    }
                }

                if (aboutImgsja_JPList != null) {
                    foreach (var item in aboutImgsja_JPList)
                    {
                        if (collection["AboutImgsja_JP[" + item + "].Status"] != null)
                        {
                            var status = collection["AboutImgsja_JP[" + item + "].Status"].Split(',')[0];
                            var des = collection["AboutImgsja_JP[" + item + "].Description"];
                            HttpPostedFileBase uploadFile = Request.Files["AboutImgsja_JP[" + item + "].ImageFile"];
                            var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                            var filename = "";
                            if (ImgHasFile)
                            {
                                ImageHelper.DeletePhoto(PhotoFolder, collection["AboutImgsja_JP[" + item + "].Image"]);
                                filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid() + "-Imageja");
                            }
                            model.AboutTranslationViewJp.AboutImgs.Add(new AboutImgView()
                            {
                                Title = collection["AboutImgsja_JP[" + item + "].Title"],
                                Sort = Convert.ToInt32(collection["AboutImgsja_JP[" + item + "].Sort"]),
                                Status = Convert.ToBoolean(status),
                                Description = des,
                                AboutTranslationsId = model.AboutTranslationViewJp.ID,
                                Image = String.IsNullOrEmpty(filename) ? collection["AboutImgsja_JP[" + item + "].Image"] : filename,
                                Language = Language.ja_JP
                            });
                        }
                    }
                }
                if (aboutImgsko_KRList != null)
                {
                    foreach (var item in aboutImgsko_KRList)
                    {
                        if (collection["AboutImgsko_KR[" + item + "].Status"] != null)
                        {
                            var status = collection["AboutImgsko_KR[" + item + "].Status"].Split(',')[0];
                            var des = collection["AboutImgsko_KR[" + item + "].Description"];
                            HttpPostedFileBase uploadFile = Request.Files["AboutImgsko_KR[" + item + "].ImageFile"];
                            var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                            var filename = "";
                            if (ImgHasFile)
                            {
                                ImageHelper.DeletePhoto(PhotoFolder, collection["AboutImgsko_KR[" + item + "].Image"]);
                                filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid() + "-Imagekr");
                            }
                            model.AboutTranslationViewKr.AboutImgs.Add(new AboutImgView()
                            {
                                Title = collection["AboutImgsko_KR[" + item + "].Title"],
                                Sort = Convert.ToInt32(collection["AboutImgsko_KR[" + item + "].Sort"]),
                                Status = Convert.ToBoolean(status),
                                Description = des,
                                AboutTranslationsId = model.AboutTranslationViewKr.ID,
                                Image = String.IsNullOrEmpty(filename) ? collection["AboutImgsko_KR[" + item + "].Image"] : filename,
                                Language = Language.ko_KR
                            });
                        }
                    }
                }
                if (aboutImgsen_USList != null) {
                    foreach (var item in aboutImgsen_USList)
                    {
                        if (collection["AboutImgsen_US[" + item + "].Status"] != null)
                        {
                            var status = collection["AboutImgsen_US[" + item + "].Status"].Split(',')[0];
                            var des = collection["AboutImgsen_US[" + item + "].Description"];
                            HttpPostedFileBase uploadFile = Request.Files["AboutImgsen_US[" + item + "].ImageFile"];
                            var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                            var filename = "";
                            if (ImgHasFile)
                            {
                                ImageHelper.DeletePhoto(PhotoFolder, collection["AboutImgsen_US[" + item + "].Image"]);
                                filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid() + "-Imageus");
                            }
                            model.AboutTranslationViewUs.AboutImgs.Add(new AboutImgView()
                            {
                                Title = collection["AboutImgsen_US[" + item + "].Title"],
                                Sort = Convert.ToInt32(collection["AboutImgsen_US[" + item + "].Sort"]),
                                Status = Convert.ToBoolean(status),
                                Description = des,
                                AboutTranslationsId = model.AboutTranslationViewUs.ID,
                                Image = String.IsNullOrEmpty(filename) ? collection["AboutImgsen_US[" + item + "].Image"] : filename,
                                Language = Language.en_US
                            });
                        }
                    }
                }
                About data = Mapper.Map<About>(model);

                var aboutTranslations = new List<AboutTranslations>();
                var aboutTranslationTw = Mapper.Map<AboutTranslations>(model.AboutTranslationViewTw);
                var aboutTranslationCn = Mapper.Map<AboutTranslations>(model.AboutTranslationViewCn);
                var aboutTranslationUs = Mapper.Map<AboutTranslations>(model.AboutTranslationViewUs);
                var aboutTranslationJp = Mapper.Map<AboutTranslations>(model.AboutTranslationViewJp);
                var aboutTranslationKr = Mapper.Map<AboutTranslations>(model.AboutTranslationViewKr);

                aboutTranslations.Add(aboutTranslationTw);
                aboutTranslations.Add(aboutTranslationCn);
                aboutTranslations.Add(aboutTranslationUs);
                aboutTranslations.Add(aboutTranslationJp);
                aboutTranslations.Add(aboutTranslationKr);

                var aboutImgTw = Mapper.Map<List<AboutImg>>(model.AboutTranslationViewTw.AboutImgs);
                var aboutImgCn = Mapper.Map<List<AboutImg>>(model.AboutTranslationViewCn.AboutImgs);
                var aboutImgUs = Mapper.Map<List<AboutImg>>(model.AboutTranslationViewUs.AboutImgs);
                var aboutImgJp = Mapper.Map<List<AboutImg>>(model.AboutTranslationViewJp.AboutImgs);
                var aboutImgKr = Mapper.Map<List<AboutImg>>(model.AboutTranslationViewKr.AboutImgs);

                
                if (aboutImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.AboutImg);
                    data.AboutImg = ImageHelper.SaveImage(PhotoFolder, model.AboutImgFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-AboutImg");
                }
                if (contactImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.ContactImg);
                    data.ContactImg = ImageHelper.SaveImage(PhotoFolder, model.ContactImgFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-ContactImg");
                }
                if (bannreImgHasFile) {
                    ImageHelper.DeletePhoto(PhotoFolder, data.BannerImg);
                    data.BannerImg = ImageHelper.SaveImage(PhotoFolder, model.BannerImgFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-BannerImg");
                }


                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;

                aboutRepository.Update(data, aboutTranslations, aboutImgTw, aboutImgCn, aboutImgUs, aboutImgKr, aboutImgJp);
                ShowMessage(true, "儲存成功");
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult AboutImgs(int language)
        {
            return PartialView("_AboutImg", new AboutImgView((Language)language));
        }

        public ActionResult DeleteImage(int type, int id = 0, string imagefile = "")
        {
            string image;
            //type 1:BannerImg 2:AboutImg 3.ContactImg
            if (type != 0)
            {
                image = aboutRepository.DeleteImage(id, type);
                ImageHelper.DeletePhoto(PhotoFolder, image);
            }
            else
            {
                aboutRepository.DeleteAboutImages(imagefile);
                ImageHelper.DeletePhoto(PhotoFolder, imagefile);
            }

            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }
    }
}
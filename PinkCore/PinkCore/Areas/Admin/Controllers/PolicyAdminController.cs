﻿using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Areas.Admin.ViewModels.Policy;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class PolicyAdminController : BaseAdminController
    {
        private PolicyRepository policyRepository = new PolicyRepository();

        // GET: Admin/PolicyAdmin
        public ActionResult Edit(int id = 0)
        {
            var model = new PolicyView();
            var hasdata = policyRepository.GetAll().Where(p => p.ID == id).FirstOrDefault();
            if (id == 2 && hasdata == null) {
                Policy data = Mapper.Map<Policy>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                var policyTranslations = new List<PolicyTranslations>();
                var policyTranslationTw = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewTw);
                var policyTranslationCn = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewCn);
                var policyTranslationUs = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewUs);
                var policyTranslationJp = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewJp);
                var policyTranslationKr = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewKr);

                policyTranslations.Add(policyTranslationTw);
                policyTranslations.Add(policyTranslationCn);
                policyTranslations.Add(policyTranslationUs);
                policyTranslations.Add(policyTranslationJp);
                policyTranslations.Add(policyTranslationKr);
                data.Creater = adminId;
                model.ID = policyRepository.Insert(data, policyTranslations);
                return RedirectToAction("Edit", new { id = model.ID });
            }
            if (id != 0 && hasdata != null)
            {
                model = policyRepository.FindById(id);
            }
            else {
                return RedirectToAction("Index", "HomeAdmin");

            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PolicyView model)
        {

            if (ModelState.IsValid)
            {
                Policy data = Mapper.Map<Policy>(model);

                var policyTranslations = new List<PolicyTranslations>();
                var policyTranslationTw = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewTw);
                var policyTranslationCn = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewCn);
                var policyTranslationUs = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewUs);
                var policyTranslationJp = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewJp);
                var policyTranslationKr = Mapper.Map<PolicyTranslations>(model.PolicyTranslationViewKr);

                policyTranslations.Add(policyTranslationTw);
                policyTranslations.Add(policyTranslationCn);
                policyTranslations.Add(policyTranslationUs);
                policyTranslations.Add(policyTranslationJp);
                policyTranslations.Add(policyTranslationKr);

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;

                //if (model.ID == 0)
                //{
                //    data.Creater = adminId;
                //    model.ID = policyRepository.Insert(data, policyTranslations);
                //}
                //else
                //{
                    policyRepository.Update(data, policyTranslations);
                //}
                ShowMessage(true, "儲存成功");
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            policyRepository.Delete(id);
            ShowMessage(true, "刪除成功");

            return RedirectToAction("Index");
        }
    }
}
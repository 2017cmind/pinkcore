﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Areas.Admin.ViewModels.Banner;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class BannerAdminController : BaseAdminController
    {
        private BannerRepository bannerRepository = new BannerRepository();

        // GET: Admin/BannerAdmin
        public ActionResult Index(BannerIndexView model)
        {
            var query = bannerRepository.Query(model.Description, (int)Language.zh_TW, model.Status);
            var pageResult = query.ToPageResult<RelBannerBannerTranslations>(model);
            model.PageResult = Mapper.Map<PageResult<BannerIndexItemView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new BannerView();
            if (id != 0)
            {
                model = bannerRepository.FindBy(id);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BannerView model)
        {
            var iconHasFile = ImageHelper.CheckFileExists(model.IconFile);
            var pcImageHasFile = ImageHelper.CheckFileExists(model.PCImageFile);
            var moblieImageHasFile = ImageHelper.CheckFileExists(model.MoblieImageFile);
            var hoverImageHasFile = ImageHelper.CheckFileExists(model.HoverImageFile);


            if (ModelState.IsValid)
            {
                Banner data = Mapper.Map<Banner>(model);

                var bannerTranslations = new List<BannerTranslations>();
                var bannerTranslationTw = Mapper.Map<BannerTranslations>(model.BannerTranslationViewTw);
                var bannerTranslationCn = Mapper.Map<BannerTranslations>(model.BannerTranslationViewCn);
                var bannerTranslationUs = Mapper.Map<BannerTranslations>(model.BannerTranslationViewUs);
                var bannerTranslationJp = Mapper.Map<BannerTranslations>(model.BannerTranslationViewJp);
                var bannerTranslationKr = Mapper.Map<BannerTranslations>(model.BannerTranslationViewKr);

                bannerTranslations.Add(bannerTranslationTw);
                bannerTranslations.Add(bannerTranslationCn);
                bannerTranslations.Add(bannerTranslationUs);
                bannerTranslations.Add(bannerTranslationJp);
                bannerTranslations.Add(bannerTranslationKr);

                var twPCImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewTw.PCImageFile);
                var cnPCImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewCn.PCImageFile);
                var usPCImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewUs.PCImageFile);
                var jpPCImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewJp.PCImageFile);
                var krPCImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewKr.PCImageFile);

                var twMoblieImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewTw.MoblieImageFile);
                var cnMoblieImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewCn.MoblieImageFile);
                var usMoblieImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewUs.MoblieImageFile);
                var jpMoblieImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewJp.MoblieImageFile);
                var krMoblieImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewKr.MoblieImageFile);

                var twHoverImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewTw.HoverImageFile);
                var cnHoverImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewCn.HoverImageFile);
                var usHoverImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewUs.HoverImageFile);
                var jpHoverImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewJp.HoverImageFile);
                var krHoverImageHasFile = ImageHelper.CheckFileExists(model.BannerTranslationViewKr.HoverImageFile);

                if (twPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationTw.PCImage);
                    bannerTranslationTw.PCImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewTw.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagezh");
                }
                if (cnPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationCn.PCImage);
                    bannerTranslationCn.PCImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewCn.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagecn");
                }
                if (usPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationUs.PCImage);
                    bannerTranslationUs.PCImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewUs.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImageus");
                }
                if (jpPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationJp.PCImage);
                    bannerTranslationJp.PCImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewJp.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagejp");
                }
                if (krPCImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationKr.PCImage);
                    bannerTranslationKr.PCImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewKr.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagekr");
                }

                if (twMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationTw.MoblieImage);
                    bannerTranslationTw.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewTw.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagezh");
                }
                if (cnMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationCn.MoblieImage);
                    bannerTranslationCn.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewCn.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagecn");
                }
                if (usMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationUs.MoblieImage);
                    bannerTranslationUs.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewUs.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImageus");
                }
                if (jpMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationJp.MoblieImage);
                    bannerTranslationJp.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewJp.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagejp");
                }
                if (krMoblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationKr.MoblieImage);
                    bannerTranslationKr.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewKr.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImagekr");
                }

                if (twHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationTw.HoverImage);
                    bannerTranslationTw.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewTw.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagezh");
                }
                if (cnHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationCn.HoverImage);
                    bannerTranslationCn.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewCn.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagecn");
                }
                if (usHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationUs.HoverImage);
                    bannerTranslationUs.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewUs.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImageus");
                }
                if (jpHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationJp.HoverImage);
                    bannerTranslationJp.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewJp.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagejp");
                }
                if (krHoverImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, bannerTranslationKr.HoverImage);
                    bannerTranslationKr.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.BannerTranslationViewKr.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagekr");
                }

                if (iconHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.Icon);
                    data.Icon = ImageHelper.SaveImage(PhotoFolder, model.IconFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Icon");
                }
                if (pcImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.PCImage);
                    data.PCImage = ImageHelper.SaveImage(PhotoFolder, model.PCImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImage");
                }
                if (moblieImageHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.MoblieImage);
                    data.MoblieImage = ImageHelper.SaveImage(PhotoFolder, model.MoblieImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-MoblieImage");
                }
                if (hoverImageHasFile) {
                    ImageHelper.DeletePhoto(PhotoFolder, data.HoverImage);
                    data.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImage");
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;

                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    model.ID = bannerRepository.Insert(data, bannerTranslations);
                }
                else
                {
                    bannerRepository.Update(data, bannerTranslations);
                }
                ShowMessage(true, "儲存成功");
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            //1.電腦版 2.手機版 3.電腦板替換圖 0.icon
            DeleteImage(id, (int)DeviceType.PC);
            DeleteImage(id, (int)DeviceType.Moblie);
            DeleteImage(id, 0);
            DeleteImage(id, (int)DeviceType.PCHover);

            var pcImages = bannerRepository.DeletePCImages(id);
            foreach (var item in pcImages)
            {
                ImageHelper.DeletePhoto(PhotoFolder, item);
            }
            var moblieImages = bannerRepository.DeleteMoblieImages(id);
            foreach (var item in moblieImages)
            {
                ImageHelper.DeletePhoto(PhotoFolder, item);
            }

            var hoverImages = bannerRepository.DeleteHoverImages(id);
            foreach (var item in hoverImages)
            {
                ImageHelper.DeletePhoto(PhotoFolder, item);
            }
            bannerRepository.Delete(id);
            ShowMessage(true, "刪除成功");

            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, int type, int language = 0)
        {
            string image;
            //type 1:PCImage , 2.Moblie  0.icon     
            if (language != 0)
            {
                image = bannerRepository.DeleteImage(id, type, language);
            }
            else
            {
                image = bannerRepository.DeleteImage(id, type);
            }
            ImageHelper.DeletePhoto(PhotoFolder, image);

            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }
    }
}
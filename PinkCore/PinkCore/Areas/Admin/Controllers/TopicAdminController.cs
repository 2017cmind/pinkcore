﻿using AutoMapper;
using Newtonsoft.Json;
using PinkCore.ActionFilters;
using PinkCore.Areas.Admin.ViewModels.Topic;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class TopicAdminController : BaseAdminController
    {
        private TopicRepository topicRepository = new TopicRepository();

        // GET: Admin/TopicAdmin
        public ActionResult Index(TopicIndexView model)
        {
            var query = topicRepository.Query(model.Title, model.ProductId, model.Status, (int)Language.zh_TW);
            var pageResult = query.ToPageResult<RelTopicTopicTranslations>(model);
            model.PageResult = Mapper.Map<PageResult<TopicIndexItemView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new TopicView();
            if (id != 0)
            {
                model = topicRepository.FindBy(id);
                DateTime dtCurrentTime = Convert.ToDateTime(model.OnlineTime);
                model.OnlineTime = dtCurrentTime.ToString("yyyy/MM/dd HH:mm");
            }
            else {
                DateTime dtCurrentTime;
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
                dtCurrentTime = TimeZoneInfo.ConvertTime(DateTime.Now, tzi);
                model.OnlineTime = dtCurrentTime.ToString("yyyy/MM/dd");
            }
            return View(model);
        }

        public ActionResult TopicInformation(int language)
        {
            return PartialView("_TopicInformation", new TopicInformationsView((Language)language));
        }

        public ActionResult TopicImgs(string collectionName)
        {
            ViewData["CollectionName"] = collectionName;
            return PartialView("_TopicImg", new TopicImgView());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(TopicView model, FormCollection collection)
        {
            var tw_query = !string.IsNullOrEmpty(model.TopicTranslationViewTw.Title) ? topicRepository.FindIdByTitle(model.TopicTranslationViewTw.Title) : null;
            var jp_query = !string.IsNullOrEmpty(model.TopicTranslationViewJp.Title) ? topicRepository.FindIdByTitle(model.TopicTranslationViewJp.Title) : null;
            var kr_query = !string.IsNullOrEmpty(model.TopicTranslationViewKr.Title) ? topicRepository.FindIdByTitle(model.TopicTranslationViewKr.Title) : null;
            var cn_query = !string.IsNullOrEmpty(model.TopicTranslationViewCn.Title) ? topicRepository.FindIdByTitle(model.TopicTranslationViewCn.Title) : null;
            var us_query = !string.IsNullOrEmpty(model.TopicTranslationViewUs.Title) ? topicRepository.FindIdByTitle(model.TopicTranslationViewUs.Title) : null;

            bool isrepeat = false;
            if (tw_query != null || jp_query != null || kr_query != null || cn_query != null || us_query != null) 
            {
                var tw_title = model.TopicTranslationViewTw.Title;
                model.TopicTranslationViewTw.Title = model.TopicTranslationViewCn.Title == model.TopicTranslationViewTw.Title ? model.TopicTranslationViewTw.Title + " 繁" : model.TopicTranslationViewTw.Title;
                model.TopicTranslationViewCn.Title = model.TopicTranslationViewCn.Title == tw_title ? model.TopicTranslationViewCn.Title + " 簡" : model.TopicTranslationViewCn.Title;

                if (model.TopicTranslationViewTw.ID != 0)
                {
                    isrepeat = (tw_query != null && tw_query?.ID != model.TopicTranslationViewTw.ID)|| 
                            (jp_query != null && jp_query?.ID != model.TopicTranslationViewJp.ID) ||
                            (kr_query != null && kr_query?.ID != model.TopicTranslationViewKr.ID) ||
                            (cn_query != null && cn_query?.ID != model.TopicTranslationViewCn.ID) ||
                            (us_query!= null && us_query?.ID != model.TopicTranslationViewUs.ID);
                }
                else {
                    isrepeat = tw_query != null || jp_query != null || kr_query != null || cn_query != null || us_query != null;
                }

                if (isrepeat) {
                    ShowMessage(false, "標題不能跟其他話題重複");
                }
            }

            var imgHasFile = ImageHelper.CheckFileExists(model.ImageFile);
            var hoverimgHasFile = ImageHelper.CheckFileExists(model.HoverImageFile);
            var topicInformationszh_TWList = collection["TopicInformationszh_TW.Index"] != null ? collection["TopicInformationszh_TW.Index"].Split(',') : null;
            var topicInformationszh_CNList = collection["TopicInformationszh_CN.Index"] != null ? collection["TopicInformationszh_CN.Index"].Split(',') : null;
            var topicInformationsen_USList = collection["TopicInformationsen_US.Index"] != null ? collection["TopicInformationsen_US.Index"].Split(',') : null;
            var topicInformationsja_JPList = collection["TopicInformationsja_JP.Index"] != null ? collection["TopicInformationsja_JP.Index"].Split(',') : null;
            var topicInformationsko_KRList = collection["TopicInformationsko_KR.Index"] != null ? collection["TopicInformationsko_KR.Index"].Split(',') : null;

            if (topicInformationszh_TWList != null)
            {
                foreach (var item in topicInformationszh_TWList)
                {
                    if (collection["TopicInformationszh_TW[" + item + "].Status"] != null)
                    {
                        var status = collection["TopicInformationszh_TW[" + item + "].Status"].Split(',')[0];
                        var des = collection["TopicInformationszh_TW[" + item + "].Content"];
                        var topicimgs = collection["TopicInformationszh_TW[" + item + "].TopicImgs.Index"] != null ? collection["TopicInformationszh_TW[" + item + "].TopicImgs.Index"].Split(',') : null;
                        var imgsList = new List<TopicImgView>();
                        if (topicimgs != null)
                        {
                            foreach (var imgItem in topicimgs)
                            {
                                var statusimg = collection["TopicInformationszh_TW[" + item + "].TopicImgs[" + imgItem + "].Status"].Split(',')[0];
                                HttpPostedFileBase uploadFile = Request.Files["TopicInformationszh_TW[" + item + "].TopicImgs[" + imgItem + "].ImageFile"];
                                var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                                var filename = "";
                                if (ImgHasFile)
                                {
                                    ImageHelper.DeletePhoto(PhotoFolder, collection["TopicInformationszh_TW[" + item + "].TopicImgs[" + imgItem + "].Image"]);
                                    filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid() + "-Imagezh");
                                }
                                imgsList.Add(new TopicImgView()
                                {
                                    Image = string.IsNullOrEmpty(filename) ? collection["TopicInformationszh_TW[" + item + "].TopicImgs[" + imgItem + "].Image"] : filename,
                                    Language = (int)Language.zh_TW,
                                    Sort = Convert.ToInt32(collection["TopicInformationszh_TW[" + item + "].TopicImgs[" + imgItem + "].Sort"]),
                                    Status = Convert.ToBoolean(statusimg),
                                });
                            }
                        }

                        model.TopicTranslationViewTw.TopicInformations.Add(new TopicInformationsView()
                        {
                            Title = collection["TopicInformationszh_TW[" + item + "].Title"],
                            Sort = Convert.ToInt32(collection["TopicInformationszh_TW[" + item + "].Sort"]),
                            Status = Convert.ToBoolean(status),
                            Arrangement = Convert.ToInt32(collection["TopicInformationszh_TW[" + item + "].Arrangement"]),
                            TopicTranslationsId = model.TopicTranslationViewTw.ID,
                            Content = des,
                            Language = Language.zh_TW,
                            VideoUrl = collection["TopicInformationszh_TW[" + item + "].VideoUrl"],
                            TopicImgs = imgsList
                        });
                    }
                }
            }

            if (topicInformationszh_CNList != null)
            {
                foreach (var item in topicInformationszh_CNList)
                {
                    if (collection["TopicInformationszh_CN[" + item + "].Status"] != null)
                    {
                        var status = collection["TopicInformationszh_CN[" + item + "].Status"].Split(',')[0];
                        var des = collection["TopicInformationszh_CN[" + item + "].Content"];
                        var topicimgs = collection["TopicInformationszh_CN[" + item + "].TopicImgs.Index"] != null ? collection["TopicInformationszh_CN[" + item + "].TopicImgs.Index"].Split(',') : null;
                        var imgsList = new List<TopicImgView>();
                        if (topicimgs != null)
                        {
                            foreach (var imgItem in topicimgs)
                            {
                                var statusimg = collection["TopicInformationszh_CN[" + item + "].TopicImgs[" + imgItem + "].Status"].Split(',')[0];
                                HttpPostedFileBase uploadFile = Request.Files["TopicInformationszh_CN[" + item + "].TopicImgs[" + imgItem + "].ImageFile"];
                                var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                                var filename = "";
                                if (ImgHasFile)
                                {
                                    ImageHelper.DeletePhoto(PhotoFolder, collection["TopicInformationszh_CN[" + item + "].TopicImgs[" + imgItem + "].Image"]);
                                    filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid().ToString());
                                }
                                imgsList.Add(new TopicImgView()
                                {
                                    Image = string.IsNullOrEmpty(filename) ? collection["TopicInformationszh_CN[" + item + "].TopicImgs[" + imgItem + "].Image"] : filename,
                                    Language = (int)Language.zh_CN,
                                    Sort = Convert.ToInt32(collection["TopicInformationszh_CN[" + item + "].TopicImgs[" + imgItem + "].Sort"]),
                                    Status = Convert.ToBoolean(statusimg),
                                });
                            }
                        }

                        model.TopicTranslationViewCn.TopicInformations.Add(new TopicInformationsView()
                        {
                            Title = collection["TopicInformationszh_CN[" + item + "].Title"],
                            Sort = Convert.ToInt32(collection["TopicInformationszh_CN[" + item + "].Sort"]),
                            Status = Convert.ToBoolean(status),
                            Arrangement = Convert.ToInt32(collection["TopicInformationszh_CN[" + item + "].Arrangement"]),
                            TopicTranslationsId = model.TopicTranslationViewTw.ID,
                            Content = des,
                            Language = Language.zh_CN,
                            VideoUrl = collection["TopicInformationszh_CN[" + item + "].VideoUrl"],
                            TopicImgs = imgsList
                        });
                    }
                }
            }
            if (topicInformationsen_USList != null)
            {
                foreach (var item in topicInformationsen_USList)
                {
                    if (collection["TopicInformationsen_US[" + item + "].Status"] != null)
                    {
                        var status = collection["TopicInformationsen_US[" + item + "].Status"].Split(',')[0];
                        var des = collection["TopicInformationsen_US[" + item + "].Content"];
                        var topicimgs = collection["TopicInformationsen_US[" + item + "].TopicImgs.Index"] != null ? collection["TopicInformationsen_US[" + item + "].TopicImgs.Index"].Split(',') : null;
                        var imgsList = new List<TopicImgView>();
                        if (topicimgs != null)
                        {
                            foreach (var imgItem in topicimgs)
                            {
                                var statusimg = collection["TopicInformationsen_US[" + item + "].TopicImgs[" + imgItem + "].Status"].Split(',')[0];
                                HttpPostedFileBase uploadFile = Request.Files["TopicInformationsen_US[" + item + "].TopicImgs[" + imgItem + "].ImageFile"];
                                var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                                var filename = "";
                                if (ImgHasFile)
                                {
                                    ImageHelper.DeletePhoto(PhotoFolder, collection["TopicInformationsen_US[" + item + "].TopicImgs[" + imgItem + "].Image"]);
                                    filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid().ToString());
                                }
                                imgsList.Add(new TopicImgView()
                                {
                                    Image = string.IsNullOrEmpty(filename) ? collection["TopicInformationsen_US[" + item + "].TopicImgs[" + imgItem + "].Image"] : filename,
                                    Language = (int)Language.en_US,
                                    Sort = Convert.ToInt32(collection["TopicInformationsen_US[" + item + "].TopicImgs[" + imgItem + "].Sort"]),
                                    Status = Convert.ToBoolean(statusimg),
                                });
                            }
                        }

                        model.TopicTranslationViewUs.TopicInformations.Add(new TopicInformationsView()
                        {
                            Title = collection["TopicInformationsen_US[" + item + "].Title"],
                            Sort = Convert.ToInt32(collection["TopicInformationsen_US[" + item + "].Sort"]),
                            Status = Convert.ToBoolean(status),
                            Arrangement = Convert.ToInt32(collection["TopicInformationsen_US[" + item + "].Arrangement"]),
                            TopicTranslationsId = model.TopicTranslationViewTw.ID,
                            Content = des,
                            Language = Language.en_US,
                            VideoUrl = collection["TopicInformationsen_US[" + item + "].VideoUrl"],
                            TopicImgs = imgsList
                        });
                    }
                }
            }

            if (topicInformationsja_JPList != null)
            {
                foreach (var item in topicInformationsja_JPList)
                {
                    if (collection["TopicInformationsja_JP[" + item + "].Status"] != null)
                    {
                        var status = collection["TopicInformationsja_JP[" + item + "].Status"].Split(',')[0];
                        var des = collection["TopicInformationsja_JP[" + item + "].Content"];
                        var topicimgs = collection["TopicInformationsja_JP[" + item + "].TopicImgs.Index"] != null ? collection["TopicInformationsja_JP[" + item + "].TopicImgs.Index"].Split(',') : null;
                        var imgsList = new List<TopicImgView>();
                        if (topicimgs != null)
                        {
                            foreach (var imgItem in topicimgs)
                            {
                                var statusimg = collection["TopicInformationsja_JP[" + item + "].TopicImgs[" + imgItem + "].Status"].Split(',')[0];
                                HttpPostedFileBase uploadFile = Request.Files["TopicInformationsja_JP[" + item + "].TopicImgs[" + imgItem + "].ImageFile"];
                                var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                                var filename = "";
                                if (ImgHasFile)
                                {
                                    ImageHelper.DeletePhoto(PhotoFolder, collection["TopicInformationsja_JP[" + item + "].TopicImgs[" + imgItem + "].Image"]);
                                    filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid().ToString());
                                }
                                imgsList.Add(new TopicImgView()
                                {
                                    Image = string.IsNullOrEmpty(filename) ? collection["TopicInformationsja_JP[" + item + "].TopicImgs[" + imgItem + "].Image"] : filename,
                                    Language = (int)Language.ja_JP,
                                    Sort = Convert.ToInt32(collection["TopicInformationsja_JP[" + item + "].TopicImgs[" + imgItem + "].Sort"]),
                                    Status = Convert.ToBoolean(statusimg),
                                });
                            }
                        }

                        model.TopicTranslationViewJp.TopicInformations.Add(new TopicInformationsView()
                        {
                            Title = collection["TopicInformationsja_JP[" + item + "].Title"],
                            Sort = Convert.ToInt32(collection["TopicInformationsja_JP[" + item + "].Sort"]),
                            Status = Convert.ToBoolean(status),
                            Arrangement = Convert.ToInt32(collection["TopicInformationsja_JP[" + item + "].Arrangement"]),
                            TopicTranslationsId = model.TopicTranslationViewTw.ID,
                            Content = des,
                            Language = Language.ja_JP,
                            VideoUrl = collection["TopicInformationsja_JP[" + item + "].VideoUrl"],
                            TopicImgs = imgsList
                        });
                    }
                }
            }

            if (topicInformationsko_KRList != null)
            {
                foreach (var item in topicInformationsko_KRList)
                {
                    if (collection["TopicInformationsko_KR[" + item + "].Status"] != null)
                    {
                        var status = collection["TopicInformationsko_KR[" + item + "].Status"].Split(',')[0];
                        var des = collection["TopicInformationsko_KR[" + item + "].Content"];
                        var topicimgs = collection["TopicInformationsko_KR[" + item + "].TopicImgs.Index"] != null ? collection["TopicInformationsko_KR[" + item + "].TopicImgs.Index"].Split(',') : null;
                        var imgsList = new List<TopicImgView>();
                        if (topicimgs != null)
                        {
                            foreach (var imgItem in topicimgs)
                            {
                                var statusimg = collection["TopicInformationsko_KR[" + item + "].TopicImgs[" + imgItem + "].Status"].Split(',')[0];
                                HttpPostedFileBase uploadFile = Request.Files["TopicInformationsko_KR[" + item + "].TopicImgs[" + imgItem + "].ImageFile"];
                                var ImgHasFile = ImageHelper.CheckFileExists(uploadFile);
                                var filename = "";
                                if (ImgHasFile)
                                {
                                    ImageHelper.DeletePhoto(PhotoFolder, collection["TopicInformationsko_KR[" + item + "].TopicImgs[" + imgItem + "].Image"]);
                                    filename = ImageHelper.SaveImage(PhotoFolder, uploadFile, Guid.NewGuid().ToString());
                                }
                                imgsList.Add(new TopicImgView()
                                {
                                    Image = string.IsNullOrEmpty(filename) ? collection["TopicInformationsko_KR[" + item + "].TopicImgs[" + imgItem + "].Image"] : filename,
                                    Language = (int)Language.ko_KR,
                                    Sort = Convert.ToInt32(collection["TopicInformationsko_KR[" + item + "].TopicImgs[" + imgItem + "].Sort"]),
                                    Status = Convert.ToBoolean(statusimg),
                                });
                            }
                        }

                        model.TopicTranslationViewKr.TopicInformations.Add(new TopicInformationsView()
                        {
                            Title = collection["TopicInformationsko_KR[" + item + "].Title"],
                            Sort = Convert.ToInt32(collection["TopicInformationsko_KR[" + item + "].Sort"]),
                            Status = Convert.ToBoolean(status),
                            Arrangement = Convert.ToInt32(collection["TopicInformationsko_KR[" + item + "].Arrangement"]),
                            TopicTranslationsId = model.TopicTranslationViewTw.ID,
                            Content = des,
                            Language = Language.ko_KR,
                            VideoUrl = collection["TopicInformationsko_KR[" + item + "].VideoUrl"],
                            TopicImgs = imgsList
                        });
                    }
                }
            }

            if (ModelState.IsValid && !isrepeat)
            {
                Topic data = Mapper.Map<Topic>(model);

                var topicTranslations = new List<TopicTranslations>();
                var topicTranslationTw = Mapper.Map<TopicTranslations>(model.TopicTranslationViewTw);
                var topicTranslationCn = Mapper.Map<TopicTranslations>(model.TopicTranslationViewCn);
                var topicTranslationUs = Mapper.Map<TopicTranslations>(model.TopicTranslationViewUs);
                var topicTranslationJp = Mapper.Map<TopicTranslations>(model.TopicTranslationViewJp);
                var topicTranslationKr = Mapper.Map<TopicTranslations>(model.TopicTranslationViewKr);

                topicTranslations.Add(topicTranslationTw);
                topicTranslations.Add(topicTranslationCn);
                topicTranslations.Add(topicTranslationUs);
                topicTranslations.Add(topicTranslationJp);
                topicTranslations.Add(topicTranslationKr);
                string pattern = @"[^\u4e00-\u9fa50-9A-Za-z\uAC00-\uD7A3\s\u0800-\u4e00]";
                if(!string.IsNullOrEmpty(topicTranslationTw.Title))
                {
                    var tw_title = Regex.Replace(topicTranslationTw.Title, pattern, "");
                    topicTranslationTw.LinkUrl = Regex.Replace(tw_title, @"\s", "-");
                    topicTranslationTw.Title = topicTranslationTw.Title.Replace(" 繁", "");
                }
                if (!string.IsNullOrEmpty(topicTranslationCn.Title))
                {
                    var cn_title = Regex.Replace(topicTranslationCn.Title, pattern, "");
                    topicTranslationCn.LinkUrl = Regex.Replace(cn_title, @"\s", "-");
                    topicTranslationCn.Title = topicTranslationCn.Title.Replace(" 簡", "");
                }
                if (!string.IsNullOrEmpty(topicTranslationUs.Title))
                {
                    var us_title = Regex.Replace(topicTranslationUs.Title, pattern, "");
                    topicTranslationUs.LinkUrl = Regex.Replace(us_title, @"\s", "-");
                }
                if (!string.IsNullOrEmpty(topicTranslationJp.Title))
                {
                    var jp_title = Regex.Replace(topicTranslationJp.Title, pattern, "");
                    topicTranslationJp.LinkUrl = Regex.Replace(jp_title, @"\s", "-");
                }
                if (!string.IsNullOrEmpty(topicTranslationKr.Title))
                {
                    var kr_title = Regex.Replace(topicTranslationKr.Title, pattern, "");
                    topicTranslationKr.LinkUrl = Regex.Replace(kr_title, @"\s", "-");
                }

                var twImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewTw.ImageFile);
                var cnImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewCn.ImageFile);
                var usImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewUs.ImageFile);
                var jpImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewJp.ImageFile);
                var krImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewKr.ImageFile);

                var twHoverImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewTw.HoverImageFile);
                var cnHoverImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewCn.HoverImageFile);
                var usHoverImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewUs.HoverImageFile);
                var jpHoverImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewJp.HoverImageFile);
                var krHoverImgHasFile = ImageHelper.CheckFileExists(model.TopicTranslationViewKr.HoverImageFile);

                if (twImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationTw.Image);
                    topicTranslationTw.Image = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewTw.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagezh");
                }
                if (cnImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationCn.Image);
                    topicTranslationCn.Image = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewCn.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagecn");
                }
                if (usImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationUs.Image);
                    topicTranslationUs.Image = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewUs.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImageus");
                }
                if (jpImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationJp.Image);
                    topicTranslationJp.Image = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewJp.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagejp");
                }
                if (krImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationKr.Image);
                    topicTranslationKr.Image = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewKr.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-PCImagekr");
                }

                if (twHoverImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationTw.HoverImage);
                    topicTranslationTw.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewTw.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagezh");
                }
                if (cnHoverImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationCn.HoverImage);
                    topicTranslationCn.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewCn.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagecn");
                }
                if (usHoverImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationUs.HoverImage);
                    topicTranslationUs.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewUs.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImageus");
                }
                if (jpHoverImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationJp.HoverImage);
                    topicTranslationJp.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewJp.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagejp");
                }
                if (krHoverImgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, topicTranslationKr.HoverImage);
                    topicTranslationKr.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.TopicTranslationViewKr.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImagekr");
                }

                if (imgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.Image);
                    data.Image = ImageHelper.SaveImage(PhotoFolder, model.ImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-Image");
                }
                if (hoverimgHasFile)
                {
                    ImageHelper.DeletePhoto(PhotoFolder, data.HoverImage);
                    data.HoverImage = ImageHelper.SaveImage(PhotoFolder, model.HoverImageFile, DateTime.Now.ToString("yyyyMMddhhmmssfff") + "-HoverImage");
                }

                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                data.Updater = adminId;
                DateTime onlineTime = Convert.ToDateTime(model.OnlineTime);
                data.OnlineTime = onlineTime;
                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    model.ID = topicRepository.Insert(data, topicTranslations);
                }
                else
                {
                    topicRepository.Update(data, topicTranslations);
                }
                SetJSon();
                ShowMessage(true, "儲存成功");
                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }
        public ActionResult Delete(int id)
        {
            //1.電腦版 3.電腦版替換圖 
            var image = topicRepository.DeleteImage(id, (int)DeviceType.PC);
            var hoverImage = topicRepository.DeleteImage(id, 0);

            ImageHelper.DeletePhoto(PhotoFolder, image);
            ImageHelper.DeletePhoto(PhotoFolder, hoverImage);

            var images = topicRepository.DeleteImages(id);
            foreach (var item in images)
            {
                ImageHelper.DeletePhoto(PhotoFolder, item);
            }
            var hoverImages = topicRepository.DeleteHoverImages(id);
            foreach (var item in hoverImages)
            {
                ImageHelper.DeletePhoto(PhotoFolder, item);
            }
            topicRepository.Delete(id);
            ShowMessage(true, "刪除成功");

            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, int type, int language = 0, int topicId = 0)
        {
            string image = "" ;
            //type 1:電腦圖  3.電腦版替換圖    
            if (language != 0 && type != 0)
            {
                image = topicRepository.DeleteImage(id, type, language);
            }
            else if (language == 0 && type != 0)
            {
                image = topicRepository.DeleteImage(id, type);
            }
            else if (type == 0)
            {
                image = topicRepository.DeleteSliderImage(id);
                id = topicId;
            }
            ImageHelper.DeletePhoto(PhotoFolder, image);

            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }

        public void SetJSon()
        {
            //繁中
            string twpath = Path.Combine(Server.MapPath("~/Content/pinkcore/js"), "seo_home_tw.json");
            var twQuery = topicRepository.List(0, Language.zh_TW, "").ToList();
            //開始拼接字串
            StringBuilder twSb = new StringBuilder();
            twSb.AppendLine("{");
            twSb.AppendLine("\"@context\":\"https://schema.org\",");
            twSb.AppendLine("\"@type\":\"ItemList\",");
            twSb.AppendLine("\"itemListElement\":[");        
            foreach (var item in twQuery.Select((Value, Index) => new { Value, Index }))
            {
                string dot = (twQuery.Count() - 1) == item.Index ? "" : ",";
                twSb.AppendLine("{\"@type\":\"ListItem\",");
                twSb.AppendLine("\"position\":" +  (item.Index +1) + ", ");
                twSb.AppendLine("\"url\":\""+ HttpContext.Request.Url.Scheme +"://" + HttpContext.Request.Url.Authority+ "/zh-TW/Topic/Detail/" + item.Value.ID + "\"}"+ dot);
            }
            twSb.AppendLine("]}");
            System.IO.File.WriteAllText(twpath, twSb.ToString(), Encoding.UTF8);

            //簡中
            string cnpath = Path.Combine(Server.MapPath("~/Content/pinkcore/js"), "seo_home_cn.json");
            var cnQuery = topicRepository.List(0, Language.zh_CN, "").ToList();
            //開始拼接字串
            StringBuilder cnSb = new StringBuilder();
            cnSb.AppendLine("{");
            cnSb.AppendLine("\"@context\":\"https://schema.org\",");
            cnSb.AppendLine("\"@type\":\"ItemList\",");
            cnSb.AppendLine("\"itemListElement\":[");
            foreach (var item in cnQuery.Select((Value, Index) => new { Value, Index }))
            {
                string dot = (cnQuery.Count() - 1) == item.Index ? "" : ",";
                cnSb.AppendLine("{\"@type\":\"ListItem\",");
                cnSb.AppendLine("\"position\":" + (item.Index + 1) + ", ");
                cnSb.AppendLine("\"url\":\"" + HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/zh-CN/Topic/Detail/" + item.Value.ID + "\"}" + dot);
            }
            cnSb.AppendLine("]}");
            System.IO.File.WriteAllText(cnpath, cnSb.ToString(), Encoding.UTF8);

            //英文
            string enpath = Path.Combine(Server.MapPath("~/Content/pinkcore/js"), "seo_home_en.json");
            var enQuery = topicRepository.List(0, Language.en_US, "").ToList();
            //開始拼接字串
            StringBuilder enSb = new StringBuilder();
            enSb.AppendLine("{");
            enSb.AppendLine("\"@context\":\"https://schema.org\",");
            enSb.AppendLine("\"@type\":\"ItemList\",");
            enSb.AppendLine("\"itemListElement\":[");
            foreach (var item in enQuery.Select((Value, Index) => new { Value, Index }))
            {
                string dot = (enQuery.Count() - 1) == item.Index ? "" : ",";
                enSb.AppendLine("{\"@type\":\"ListItem\",");
                enSb.AppendLine("\"position\":" + (item.Index + 1) + ", ");
                enSb.AppendLine("\"url\":\"" + HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/en-US/Topic/Detail/" + item.Value.ID + "\"}" + dot);
            }
            enSb.AppendLine("]}");
            System.IO.File.WriteAllText(enpath, enSb.ToString(), Encoding.UTF8);

            //日文
            string japath = Path.Combine(Server.MapPath("~/Content/pinkcore/js"), "seo_home_jp.json");
            var jaQuery = topicRepository.List(0, Language.ja_JP, "").ToList();
            //開始拼接字串
            StringBuilder jaSb = new StringBuilder();
            jaSb.AppendLine("{");
            jaSb.AppendLine("\"@context\":\"https://schema.org\",");
            jaSb.AppendLine("\"@type\":\"ItemList\",");
            jaSb.AppendLine("\"itemListElement\":[");
            foreach (var item in jaQuery.Select((Value, Index) => new { Value, Index }))
            {
                string dot = (jaQuery.Count() - 1) == item.Index ? "" : ",";
                jaSb.AppendLine("{\"@type\":\"ListItem\",");
                jaSb.AppendLine("\"position\":" + (item.Index + 1) + ", ");
                jaSb.AppendLine("\"url\":\"" + HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/ja-JP/Topic/Detail/" + item.Value.ID + "\"}" + dot);
            }
            jaSb.AppendLine("]}");
            System.IO.File.WriteAllText(japath, jaSb.ToString(), Encoding.UTF8);

            //繁中
            string krpath = Path.Combine(Server.MapPath("~/Content/pinkcore/js"), "seo_home_kr.json");
            var krQuery = topicRepository.Query("", 0, true, (int)Language.ko_KR).ToList();
            //開始拼接字串
            StringBuilder krSb = new StringBuilder();
            krSb.AppendLine("{");
            krSb.AppendLine("\"@context\":\"https://schema.org\",");
            krSb.AppendLine("\"@type\":\"ItemList\",");
            krSb.AppendLine("\"itemListElement\":[");
            foreach (var item in krQuery.Select((Value, Index) => new { Value, Index }))
            {
                string dot = (krQuery.Count() - 1) == item.Index ? "" : ",";
                krSb.AppendLine("{\"@type\":\"ListItem\",");
                krSb.AppendLine("\"position\":" + (item.Index + 1) + ", ");
                krSb.AppendLine("\"url\":\"" + HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + "/ko-KR/Topic/Detail/" + item.Value.ID + "\"}" + dot);
            }
            krSb.AppendLine("]}");
            System.IO.File.WriteAllText(krpath, krSb.ToString(), Encoding.UTF8);

        }
    }
}
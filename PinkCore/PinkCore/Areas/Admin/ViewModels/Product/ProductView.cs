﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.ViewModels.Product
{
    public class ProductView
    {
        public ProductView()
        {
            ProductTranslationViewTw = new ProductTranslationView(Language.zh_TW);
            ProductTranslationViewCn = new ProductTranslationView(Language.zh_CN);
            ProductTranslationViewUs = new ProductTranslationView(Language.en_US);
            ProductTranslationViewKr = new ProductTranslationView(Language.ko_KR);
            ProductTranslationViewJp = new ProductTranslationView(Language.ja_JP);
        }

        public int ID { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "是否為精選遊戲")]
        public bool isHot { get; set; }

        [Display(Name = "我只想看帥哥分類")]
        public bool IsLimit { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        public string[] supportLans { get; set; }

        [Display(Name = "上線日期")]
        public string OnlineTime { get; set; }


        [Display(Name = "支援語言")]
        [StringLength(125, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string SupportLan { get; set; }


        [Display(Name = "主圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "替換圖片")]
        public string HoverImage { get; set; }
        public HttpPostedFileBase HoverImageFile { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ProductType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }


        public System.DateTime CreateTime { get; set; }

        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }

        public ProductTranslationView ProductTranslationViewTw { get; set; }

        public ProductTranslationView ProductTranslationViewCn { get; set; }

        public ProductTranslationView ProductTranslationViewUs { get; set; }

        public ProductTranslationView ProductTranslationViewJp { get; set; }
        
        public ProductTranslationView ProductTranslationViewKr { get; set; }
    }

    public class ProductTranslationView
    {
        public ProductTranslationView() { }
        public ProductTranslationView(Language language)
        {
            this.Language = language;
        }
        public int ID { get; set; }
        public int ProductId { get; set; }
        public Language Language { get; set; }

        [Display(Name = "主圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "替換圖片")]
        public string HoverImage { get; set; }
        public HttpPostedFileBase HoverImageFile { get; set; }

        [Display(Name = "敘述")]
        public string Description { get; set; }

        [Display(Name = "標題")]
        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Name { get; set; }

        [Display(Name = "遊戲連結")]
        public string Link { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.ViewModels.Product
{
    public class ProductQuery : PageQuery
    {
        public ProductQuery()
        {
            this.Sorting = "OnlineTime";
            this.isDescending = true;
        }

        [Display(Name = "標題")]
        public string Name { get; set; }

        [Display(Name = "類別")]
        public ProductType Type { get; set; }

        [Display(Name = "是否為精選遊戲")]
        public bool isHot { get; set; }

        [Display(Name = "上線日期")]
        public DateTime OnlineTime { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ProductType>();
                result.Insert(0, new SelectListItem() { Value = "", Text = "全部" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.ViewModels.Product
{
    public class ProductIndexView : ProductQuery
    {
        public PageResult<ProductIndexItemView> PageResult { get; set; }
    }

    public class ProductIndexItemView
    {
        public int ID { get; set; }

        [Display(Name = "類型")]
        public ProductType Type { get; set; }

        [Display(Name = "標題")]
        public string Name { get; set; }

        [Display(Name = "是否為精選遊戲")]
        public bool isHot { get; set; }

        [Display(Name = "上線日期")]
        public DateTime OnlineTime { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }

    }
}
﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.ViewModels.Policy
{
    public class PolicyView
    {
        public PolicyView()
        {
            PolicyTranslationViewTw = new PolicyTranslationView(Language.zh_TW);
            PolicyTranslationViewCn = new PolicyTranslationView(Language.zh_CN);
            PolicyTranslationViewUs = new PolicyTranslationView(Language.en_US);
            PolicyTranslationViewKr = new PolicyTranslationView(Language.ko_KR);
            PolicyTranslationViewJp = new PolicyTranslationView(Language.ja_JP);
        }

        public int ID { get; set; }

        public System.DateTime CreateTime { get; set; }

        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }

        public PolicyTranslationView PolicyTranslationViewTw { get; set; }

        public PolicyTranslationView PolicyTranslationViewCn { get; set; }

        public PolicyTranslationView PolicyTranslationViewUs { get; set; }

        public PolicyTranslationView PolicyTranslationViewJp { get; set; }
        
        public PolicyTranslationView PolicyTranslationViewKr { get; set; }
    }

    public class PolicyTranslationView
    {
        public PolicyTranslationView() { }
        public PolicyTranslationView(Language language)
        {
            this.Language = language;
        }
        public int ID { get; set; }
        public int PolicyId { get; set; }
        public Language Language { get; set; }

        [AllowHtml]
        [Display(Name = "服務條款內容")]
        public string PolicyContent { get; set; }

        [AllowHtml]
        [Display(Name = "隱私條款內容")]
        public string PrivacyContent { get; set; }
    }
}
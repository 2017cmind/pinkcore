﻿using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PinkCore.Areas.Admin.ViewModels.Topic
{
    public class TopicIndexView : TopicQuery
    {
        public PageResult<TopicIndexItemView> PageResult { get; set; }
    }

    public class TopicIndexItemView
    {
        ProductRepository productRepository = new ProductRepository();

        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "遊戲")]
        public int ProductId { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }


        public string ProductName
        {
            get
            {
                var result = productRepository.FindBy(ProductId).ProductTranslationViewTw.Name;
                return result;
            }
        }

        [Display(Name = "上線日期")]
        public DateTime OnlineTime { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }
    }
}
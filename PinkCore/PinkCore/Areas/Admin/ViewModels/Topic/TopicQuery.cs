﻿using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.ViewModels.Topic
{
    public class TopicQuery : PageQuery
    {
        public TopicQuery()
        {
            this.Sorting = "OnlineTime";
            this.isDescending = true;
        }

        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "上線日期")]
        public DateTime OnlineTime { get; set; }

        [Display(Name = "遊戲")]
        public int ProductId { get; set; }

        [Display(Name = "啟用狀態")]
        public bool ? Status { get; set; }

        ProductRepository productRepository = new ProductRepository();

        public List<SelectListItem> ProductOptions
        {
            get
            {
                var result = new List<SelectListItem>();
                var values = productRepository.GetAll().Where(p => p.Language == (int)Language.zh_TW);
                result.Insert(0, new SelectListItem { Text = "全部", Value = "0" });
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Name
                    });
                }
                return result;
            }
        }


        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }
    }
}
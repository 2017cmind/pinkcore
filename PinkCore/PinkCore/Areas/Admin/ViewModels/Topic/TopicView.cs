﻿using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.ViewModels.Topic
{
    public class TopicView
    {
        public TopicView()
        {
            TopicTranslationViewTw = new TopicTranslationView(Language.zh_TW);
            TopicTranslationViewCn = new TopicTranslationView(Language.zh_CN);
            TopicTranslationViewUs = new TopicTranslationView(Language.en_US);
            TopicTranslationViewKr = new TopicTranslationView(Language.ko_KR);
            TopicTranslationViewJp = new TopicTranslationView(Language.ja_JP);
            ProductRepository productRepository = new ProductRepository();

            var values = productRepository.Query(null, 0,(int)Language.zh_TW, null);
            List<SelectListItem> options = values.Select(p => new SelectListItem()
            {
                Text = p.Name,
                Value = p.ID.ToString()
            }).ToList();
            this.ProductOptions = options;
        }

        public int ID { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "我只想看帥哥分類")]
        public bool IsLimit { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "電腦版替換圖")]
        public string HoverImage { get; set; }
        public HttpPostedFileBase HoverImageFile { get; set; }

        [Display(Name = "相關遊戲")]
        public int ProductId { get; set; }

        public List<SelectListItem> ProductOptions { get; set; }


        [Display(Name = "上線日期")]
        public string OnlineTime { get; set; }

        public System.DateTime CreateTime { get; set; }

        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }

        public TopicTranslationView TopicTranslationViewTw { get; set; }

        public TopicTranslationView TopicTranslationViewCn { get; set; }

        public TopicTranslationView TopicTranslationViewUs { get; set; }

        public TopicTranslationView TopicTranslationViewJp { get; set; }

        public TopicTranslationView TopicTranslationViewKr { get; set; }
    }

    public class TopicTranslationView
    {
        public TopicTranslationView() { }
        public TopicTranslationView(Language language)
        {
            this.TopicInformations = new List<TopicInformationsView>();
            this.Language = language;
        }
        public int ID { get; set; }
        public int TopicId { get; set; }
        public Language Language { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "電腦版替換圖")]
        public string HoverImage { get; set; }
        public HttpPostedFileBase HoverImageFile { get; set; }

        [Display(Name = "標題")]
        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Title { get; set; }

        [Display(Name = "前台連結")]
        public string LinkUrl { get; set; }

        [Display(Name = "標籤")]
        [StringLength(52, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Tags { get; set; }

        public List<TopicInformationsView> TopicInformations { get; set; }
    }
    public class TopicInformationsView {

        public TopicInformationsView(Language language = 0)
        {
            this.ItemIndex = Guid.NewGuid();
            this.TopicImgs = new List<TopicImgView>();
            this.Language = language;
            var values = EnumHelper.GetValues<ArrangementType>();
            List<SelectListItem> options = values.Select(p => new SelectListItem()
            {
                Text = string.Format("{0} ({1})", p, EnumHelper.GetDescription(p)),
                Value = ((int)p).ToString()
            }).ToList();
            this.ArrangementOptions = options;
        }
        public int ID { get; set; }

        public Language Language { get; set; }

        public Guid ItemIndex { get; set; }

        public int TopicTranslationsId { get; set; }

        [Display(Name = "排版類型")]
        public int Arrangement { get; set; }

        [Display(Name = "前台連結")]
        public string LinkUrl { get; set; }

        [Display(Name = "影片連結")]
        public string VideoUrl { get; set; }

        [Display(Name = "標題")]
        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Title { get; set; }

        [AllowHtml]
        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        public List<SelectListItem> ArrangementOptions { get; set; }

        public List<TopicImgView> TopicImgs { get; set; }
    }

    public class TopicImgView
    {
        public int ID { get; set; }

        public int TopicInformationId { get; set; }

        [Display(Name = "語言")]
        public int Language { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }
    }
}
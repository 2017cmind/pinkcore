﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.ViewModels.About
{
    public class AboutView
    {
        public AboutView()
        {
            AboutTranslationViewTw = new AboutTranslationView(Language.zh_TW);
            AboutTranslationViewCn = new AboutTranslationView(Language.zh_CN);
            AboutTranslationViewUs = new AboutTranslationView(Language.en_US);
            AboutTranslationViewKr = new AboutTranslationView(Language.ko_KR);
            AboutTranslationViewJp = new AboutTranslationView(Language.ja_JP);
        }

        public int ID { get; set; }

        [Display(Name = "Bannre主圖")]
        public string BannerImg { get; set; }
        public HttpPostedFileBase BannerImgFile { get; set; }

        [Display(Name = "關於我們圖片")]
        public string AboutImg { get; set; }
        public HttpPostedFileBase AboutImgFile { get; set; }

        [Display(Name = "聯絡我們手機版圖片")]
        public string ContactImg { get; set; }
        public HttpPostedFileBase ContactImgFile { get; set; }

        public System.DateTime CreateTime { get; set; }

        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }

        public AboutTranslationView AboutTranslationViewTw { get; set; }

        public AboutTranslationView AboutTranslationViewCn { get; set; }

        public AboutTranslationView AboutTranslationViewUs { get; set; }

        public AboutTranslationView AboutTranslationViewJp { get; set; }
        
        public AboutTranslationView AboutTranslationViewKr { get; set; }


    }

    public class AboutTranslationView
    {
        public AboutTranslationView() { }
        public AboutTranslationView(Language language)
        {
            this.AboutImgs = new List<AboutImgView>();
            //AboutImgsViewTw = new AboutImgView(Language.zh_TW);
            //AboutImgsViewCn = new List<AboutImgView>();
            //AboutImgsViewUs = new List<AboutImgView>();
            //AboutImgsViewKr = new List<AboutImgView>();
            //AboutImgsViewJp = new List<AboutImgView>();
            this.Language = language;
            //this.EventContactChangeFieldNames = new List<EventContactChangeFieldNameView>();
        }
        public int ID { get; set; }
        public int AboutId { get; set; }
        public Language Language { get; set; }

        [AllowHtml]
        [Display(Name = "關於我們內容")]
        public string AboutContent { get; set; }

        [Display(Name = "關於我們標題")]
        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string AboutTitle { get; set; }

        [AllowHtml]
        [Display(Name = "聯絡我們")]
        public string ContactContent { get; set; }

        [Display(Name = "聯絡我們標題")]
        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string ContactTitle { get; set; }

        public List<AboutImgView> AboutImgs { get; set; }


    }

    public class AboutImgView
    {
        public AboutImgView(Language language = 0)
        {
            //this.ItemIndex = Guid.NewGuid();
            //this.EventContactChangeFieldNames = new List<EventContactChangeFieldNameView>();
            this.Language = language;

        }
        //public Guid ItemIndex { get; set; }
        public Language Language { get; set; }

        public int ID { get; set; }
        public int AboutTranslationsId { get; set; }

        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [AllowHtml]
        [Display(Name = "敘述")]
        public string Description { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }
    }
}
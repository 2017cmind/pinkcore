﻿using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.ViewModels.AgeConfirm
{
    public class AgeConfirmView
    {
        public AgeConfirmView()
        {
            AgeConfirmTranslationViewTw = new AgeConfirmTranslationView(Language.zh_TW);
            AgeConfirmTranslationViewCn = new AgeConfirmTranslationView(Language.zh_CN);
            AgeConfirmTranslationViewUs = new AgeConfirmTranslationView(Language.en_US);
            AgeConfirmTranslationViewKr = new AgeConfirmTranslationView(Language.ko_KR);
            AgeConfirmTranslationViewJp = new AgeConfirmTranslationView(Language.ja_JP);
        }

        public int ID { get; set; }

        public System.DateTime CreateTime { get; set; }

        [Display(Name = "電腦版圖片")]
        public string PCImage { get; set; }
        public HttpPostedFileBase PCImageFile { get; set; }

        [Display(Name = "手機版圖片")]
        public string MoblieImage { get; set; }
        public HttpPostedFileBase MoblieImageFile { get; set; }

        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }

        public AgeConfirmTranslationView AgeConfirmTranslationViewTw { get; set; }

        public AgeConfirmTranslationView AgeConfirmTranslationViewCn { get; set; }

        public AgeConfirmTranslationView AgeConfirmTranslationViewUs { get; set; }

        public AgeConfirmTranslationView AgeConfirmTranslationViewJp { get; set; }

        public AgeConfirmTranslationView AgeConfirmTranslationViewKr { get; set; }
    }
    public class AgeConfirmTranslationView
    {
        public AgeConfirmTranslationView() { }
        public AgeConfirmTranslationView(Language language)
        {
            this.Language = language;
        }
        public int ID { get; set; }
        public int AgeConfirmId { get; set; }
        public Language Language { get; set; }

        [Display(Name = "電腦版圖片")]
        public string PCImage { get; set; }
        public HttpPostedFileBase PCImageFile { get; set; }

        [Display(Name = "手機版圖片")]
        public string MoblieImage { get; set; }
        public HttpPostedFileBase MoblieImageFile { get; set; }
    }
}
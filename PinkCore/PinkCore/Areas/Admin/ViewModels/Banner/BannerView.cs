﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.ViewModels.Banner
{
    public class BannerView
    {
        public BannerView()
        {
            BannerTranslationViewTw = new BannerTranslationView(Language.zh_TW);
            BannerTranslationViewCn = new BannerTranslationView(Language.zh_CN);
            BannerTranslationViewUs = new BannerTranslationView(Language.en_US);
            BannerTranslationViewKr = new BannerTranslationView(Language.ko_KR);
            BannerTranslationViewJp = new BannerTranslationView(Language.ja_JP);
        }

        public int ID { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "我只想看帥哥分類")]
        public bool IsLimit { get; set; }

        [Display(Name = "Icon圖片")]
        public string Icon { get; set; }
        public HttpPostedFileBase IconFile { get; set; }

        [Display(Name = "電腦版圖片")]
        public string PCImage { get; set; }
        public HttpPostedFileBase PCImageFile { get; set; }

        [Display(Name = "手機版圖片")]
        public string MoblieImage { get; set; }
        public HttpPostedFileBase MoblieImageFile { get; set; }

        [Display(Name = "電腦版替換圖片")]
        public string HoverImage { get; set; }
        public HttpPostedFileBase HoverImageFile { get; set; }

        public System.DateTime CreateTime { get; set; }

        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }

        public BannerTranslationView BannerTranslationViewTw { get; set; }

        public BannerTranslationView BannerTranslationViewCn { get; set; }

        public BannerTranslationView BannerTranslationViewUs { get; set; }

        public BannerTranslationView BannerTranslationViewJp { get; set; }
        
        public BannerTranslationView BannerTranslationViewKr { get; set; }
    }

    public class BannerTranslationView
    {
        public BannerTranslationView() { }
        public BannerTranslationView(Language language)
        {
            this.Language = language;
        }
        public int ID { get; set; }
        public int BannerId { get; set; }
        public Language Language { get; set; }

        [Display(Name = "Icon圖片")]
        public string Icon { get; set; }
        public HttpPostedFileBase IconFile { get; set; }

        [Display(Name = "電腦版圖片")]
        public string PCImage { get; set; }
        public HttpPostedFileBase PCImageFile { get; set; }

        [Display(Name = "手機版圖片")]
        public string MoblieImage { get; set; }
        public HttpPostedFileBase MoblieImageFile { get; set; }

        [Display(Name = "電腦版替換圖片")]
        public string HoverImage { get; set; }
        public HttpPostedFileBase HoverImageFile { get; set; }

        [Display(Name = "連結")]
        public string Link { get; set; }

        [Display(Name = "敘述")]
        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Description { get; set; }

        [Display(Name = "按鈕文字")]
        [StringLength(12, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string BtnName { get; set; }
    }
}
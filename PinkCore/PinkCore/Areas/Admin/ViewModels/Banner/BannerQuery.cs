﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;

namespace PinkCore.Areas.Admin.ViewModels.Banner
{
    public class BannerQuery : PageQuery
    {
        public BannerQuery()
        {
            this.Sorting = "Sort";
            this.isDescending = false;
        }

        [Display(Name = "電腦版圖片")]
        public string PCImage { get; set; }

        [Display(Name = "敘述")]
        public string Description { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateTime { get; set; }
    }
}
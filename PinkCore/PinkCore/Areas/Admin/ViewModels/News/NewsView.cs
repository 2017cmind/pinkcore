﻿using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Admin.ViewModels.News
{
    public class NewsView
    {
        public NewsView()
        {
            NewsTranslationViewTw = new NewsTranslationView(Language.zh_TW);
            NewsTranslationViewCn = new NewsTranslationView(Language.zh_CN);
            NewsTranslationViewUs = new NewsTranslationView(Language.en_US);
            NewsTranslationViewKr = new NewsTranslationView(Language.ko_KR);
            NewsTranslationViewJp = new NewsTranslationView(Language.ja_JP);
            ProductRepository productRepository = new ProductRepository();

            var values = productRepository.Query(null, 0, (int)Language.zh_TW, null);
            List<SelectListItem> options = values.Select(p => new SelectListItem()
            {
                Text = p.Name,
                Value = p.ID.ToString()
            }).ToList();
            this.ProductOptions = options;
        }

        public int ID { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "相關遊戲")]
        public int ProductId { get; set; }

        public List<SelectListItem> ProductOptions { get; set; }


        [Display(Name = "上線日期")]
        public string OnlineTime { get; set; }

        public System.DateTime CreateTime { get; set; }

        public int Creater { get; set; }

        [Display(Name = "更新日期")]
        public System.DateTime UpdateTime { get; set; }

        public int Updater { get; set; }

        public NewsTranslationView NewsTranslationViewTw { get; set; }

        public NewsTranslationView NewsTranslationViewCn { get; set; }

        public NewsTranslationView NewsTranslationViewUs { get; set; }

        public NewsTranslationView NewsTranslationViewJp { get; set; }

        public NewsTranslationView NewsTranslationViewKr { get; set; }
    }

    public class NewsTranslationView
    {
        public NewsTranslationView() { }
        public NewsTranslationView(Language language)
        {
            this.Language = language;
        }
        public int ID { get; set; }
        public int NewsId { get; set; }
        public Language Language { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "標題")]
        [StringLength(60, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Title { get; set; }

        [Display(Name = "前台標題參數")]
        public string LinkUrl { get; set; }      

        [Display(Name = "標籤")]
        [StringLength(50, ErrorMessage = "{0} 不能超過{1}字. ")]
        public string Tags { get; set; }

        [AllowHtml]
        [Display(Name = "內容")]
        public string Content { get; set; }

    }
}
﻿using PinkCore.ActionFilters;
using PinkCore.Areas.Darakugear.ViewModels.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Darakugear.Controllers
{
    [ErrorHandleActionFilter]
    public class CharactersController : BaseController
    {
        // GET: Darakugear/Characters
        public ActionResult Index(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Christine(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Karla(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Xinthia(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Selina(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Rita(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Jess(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Luna(CharactersView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

    }
}
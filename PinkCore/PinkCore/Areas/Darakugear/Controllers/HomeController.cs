﻿using PinkCore.ActionFilters;
using PinkCore.Areas.Darakugear.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Areas.Darakugear.Controllers
{
    [ErrorHandleActionFilter]
    public class HomeController : BaseController
    {
        // GET: Darakugear/Home
        public ActionResult Index(HomeView model)
        {
            var language = GetLanguage();
            model.Language = language;
            //var isage = IsAge();
            //if (!isage) {
            //    return RedirectToAction("Index", "Home", new { Area = "", lang = language });
            //}
            return View(model);
        }
    }
}
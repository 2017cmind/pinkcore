﻿using PinkCore.ActionFilters;
using PinkCore.Repositories;
using PinkCore.Areas.Darakugear.ViewModels.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;

namespace PinkCore.Areas.Darakugear.Controllers
{
    [ErrorHandleActionFilter]
    public class NewsController : BaseController
    {
        private NewsRepository newsRepository = new NewsRepository();
        // GET: Darakugear/News
        public ActionResult Index(NewsView model)
        {
            var language = GetLanguage();
            //2為墮落機甲遊戲ID
            var query = newsRepository.List(language, 2);
            model.DataList = Mapper.Map<IEnumerable<NewsItem>>(query);
            //var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            return View(model);
        }

        public ActionResult Detail(string id)
        {
            var language = GetLanguage();
            var isage = IsAge();
            //if (!isage)
            //{
            //    return RedirectToAction("Index", "Home", new { Areas = "", lang = language });
            //}
            var model = new NewsItem();

            if (string.IsNullOrEmpty(id) || id == "Index")
            {
                return RedirectToAction("Detail", new { lang = HttpContext.Request.RequestContext.RouteData.Values["lang"].ToString() });
            }

            //string name = redirectToFriendUrl(id);
            //if (!string.IsNullOrEmpty(name))
            //{
            //    return RedirectToAction("Detail", new { id = name });
            //}
            int numId = 0;
            if (!string.IsNullOrEmpty(id) && !int.TryParse(id, out numId))
            {
                var newsdata = newsRepository.FindIdByLinkTitle(id, (int)language);
                if (newsdata == null)
                {
                    newsdata = newsRepository.FindTopicIdByLinkTitle(id);
                    var newsId = newsdata.NewsId;
                    var query = newsRepository.Detail(newsId, language, 2);

                    var name = !string.IsNullOrEmpty(query.LinkUrl) ? query.LinkUrl : newsId.ToString();

                    return RedirectToAction("Detail", new { id = name });
                }
                else
                {
                    var newsId = newsdata.NewsId;
                    var query = newsRepository.Detail(newsId, language, 2);
                    if (query != null)
                    {
                        model = Mapper.Map<NewsItem>(query);
                    }
                    else
                    {
                        return RedirectToAction("Error", new { lang = HttpContext.Request.RequestContext.RouteData.Values["lang"].ToString() });
                    }
                }

            }
            if (int.TryParse(id, out numId))
            {
                var query = newsRepository.Detail(numId, language, 2);
                if (query != null)
                {
                    if (!string.IsNullOrEmpty(query.LinkUrl))
                    {
                        return RedirectToAction("Detail", new { id = query.LinkUrl });
                    }
                    model = Mapper.Map<NewsItem>(query);
                }
                else
                {
                    return RedirectToAction("Error", new { lang = HttpContext.Request.RequestContext.RouteData.Values["lang"].ToString() });
                }
            }
            return View(model);
        }
    }
}
﻿using PinkCore.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PinkCore.Areas.Darakugear.ViewModels.News
{
    public class NewsView
    {
        public IEnumerable<NewsItem> DataList { get; set; }
    }
    public class NewsItem
    {
        public int ID { get; set; }
        public int NewsId { get; set; }
        public Language Language { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        public string TranslationsImage { get; set; }

        [Display(Name = "前台標題參數")]
        public string LinkUrl { get; set; }

        [Display(Name = "標籤")]
        public string Tags { get; set; }

        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "上線日期")]
        public DateTime OnlineTime { get; set; }
        
    }
}
﻿using System.Web.Mvc;

namespace PinkCore.Areas.Darakugear
{
    public class DarakugearAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Darakugear";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
                  "Darakugear_default",
                  "Darakugear",
                  new { controller = "Home", action = "Index", id = UrlParameter.Optional, lang = "zh-TW" },
                  new[] { "PinkCore.Areas.Darakugear.Controllers" }
            );
            context.MapRoute(
                "Darakugear",
                "Darakugear/{lang}/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional, lang = "zh-TW" },
                new[] { "PinkCore.Areas.Darakugear.Controllers" }
            );
        }
    }
}
﻿var topicInformation_zh_TW = {
    addNew: function () {
        $.get('/Admin/TopicAdmin/TopicInformation?language=1', function (template) {
            $('#js-topicinformation1').append(template);
            var arrangementlist = $('#js-topicinformation1 .js-arrangement');
            arrangementlist.change();
            var informationitems = document.getElementById("js-topicinformation1");
            var sort = informationitems.getElementsByClassName("js-sort");
            var num = 0;
            for (var i = 0; i < informationitems.childNodes.length; i++) {
                if (informationitems.childNodes[i].className == "topicInformations1") {
                    num++;
                }
            }
            initCKeditor();
            addPartialViewValid();
            sort[num - 1].value = num;
            informationitems.getElementsByClassName("topicInformationsTitle1")[(num - 1)].innerHTML = "話題區塊" + num;
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};

var topicInformation_zh_CN = {
    addNew: function () {
        $.get('/Admin/TopicAdmin/TopicInformation?language=2', function (template) {
            $('#js-topicinformation2').append(template);
            var arrangementlist = $('#js-topicinformation2 .js-arrangement');
            arrangementlist.change();
            var informationitems = document.getElementById("js-topicinformation2");
            var sort = informationitems.getElementsByClassName("js-sort");
            var num = 0;
            for (var i = 0; i < informationitems.childNodes.length; i++) {
                if (informationitems.childNodes[i].className == "topicInformations2") {
                    num++;
                }
            }
            initCKeditor();
            addPartialViewValid();
            sort[num - 1].value = num;
            informationitems.getElementsByClassName("topicInformationsTitle2")[(num - 1)].innerHTML = "話題區塊" + num;
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};
var topicInformation_en_US = {
    addNew: function () {
        $.get('/Admin/TopicAdmin/TopicInformation?language=3', function (template) {
            $('#js-topicinformation3').append(template);
            var arrangementlist = $('#js-topicinformation3 .js-arrangement');
            arrangementlist.change();
            var informationitems = document.getElementById("js-topicinformation3");
            var sort = informationitems.getElementsByClassName("js-sort");
            var num = 0;
            for (var i = 0; i < informationitems.childNodes.length; i++) {
                if (informationitems.childNodes[i].className == "topicInformations3") {
                    num++;
                }
            }
            initCKeditor();
            addPartialViewValid();
            sort[num - 1].value = num;
            informationitems.getElementsByClassName("topicInformationsTitle3")[(num - 1)].innerHTML = "話題區塊" + num;
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};

var topicInformation_ja_JP = {
    addNew: function () {
        $.get('/Admin/TopicAdmin/TopicInformation?language=4', function (template) {
            $('#js-topicinformation4').append(template);
            var arrangementlist = $('#js-topicinformation4 .js-arrangement');
            arrangementlist.change();
            var informationitems = document.getElementById("js-topicinformation4");
            var sort = informationitems.getElementsByClassName("js-sort");
            var num = 0;
            for (var i = 0; i < informationitems.childNodes.length; i++) {
                if (informationitems.childNodes[i].className == "topicInformations4") {
                    num++;
                }
            }
            initCKeditor();
            addPartialViewValid();
            sort[num - 1].value = num;
            informationitems.getElementsByClassName("topicInformationsTitle4")[(num - 1)].innerHTML = "話題區塊" + num;
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};

var topicInformation_ko_KR = {
    addNew: function () {
        $.get('/Admin/TopicAdmin/TopicInformation?language=5', function (template) {
            $('#js-topicinformation5').append(template);
            var arrangementlist = $('#js-topicinformation5 .js-arrangement');
            arrangementlist.change();
            var informationitems = document.getElementById("js-topicinformation5");
            var sort = informationitems.getElementsByClassName("js-sort");
            var num = 0;
            for (var i = 0; i < informationitems.childNodes.length; i++) {
                if (informationitems.childNodes[i].className == "topicInformations5") {
                    num++;
                }
            }
            initCKeditor();
            addPartialViewValid();
            sort[num - 1].value = num;
            informationitems.getElementsByClassName("topicInformationsTitle5")[(num - 1)].innerHTML = "話題區塊" + num;
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};

var topicImgs = {
    addNew: function (e, collectionName) {
        const $this = $(e);
        $.get('/Admin/TopicAdmin/TopicImgs', { collectionName: collectionName }, function (template) {
            var $target = $($this.parent().parent().find('.js-topicimg')[0]);
            $target.append(template);
            initCKeditor();
            addPartialViewValid();
            var arrangementlist = $($this.parent().parent().find('.js-arrangement')[0]);
            arrangementlist.change();
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};

function addPartialViewValid() {
    $("form").each(function () { $.data($(this)[0], 'validator', false); });
    $.validator.unobtrusive.parse("form");
}
var option = {
    customConfig: '/Content/Admin/ckeditor/ckeditor_custom_config.js',
};
function initCKeditor() {
    $(".js-ckeditor").each(function () {
        var id = $(this).attr('id');
        CKEDITOR.replace(id, option);
    });
}
function changeArrangement(e) {
    var $this = $(e);
    var value = $this.val();
    var $target = $($this.parents('li')[0]);
    var $topicImg = $target.find('#topicimgs');
    var $topicImgBtn = $target.find('#topicImgBtn');
    switch (value) {
        case '1':
            $topicImgBtn.hide();
            $topicImg.hide();
            break;
        case '2':
            $topicImgBtn.show();
            $topicImg.show();
            break;
    }
}
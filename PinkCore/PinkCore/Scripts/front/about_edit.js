﻿var aboutimgs_zh_TW = {
    addNew: function () {
        $.get('/Admin/AboutAdmin/AboutImgs?language=1', function (template) {
            $('#js-aboutimgs1').append(template);
            addPartialViewValid();
            initCKeditor()
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};
var aboutimgs_zh_CN = {
    addNew: function () {
        $.get('/Admin/AboutAdmin/AboutImgs?language=2', function (template) {
            $('#js-aboutimgs2').append(template);
            addPartialViewValid();
            initCKeditor()
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};
var aboutimgs_en_US = {
    addNew: function () {
        $.get('/Admin/AboutAdmin/AboutImgs?language=3', function (template) {
            $('#js-aboutimgs3').append(template);
            addPartialViewValid();
            initCKeditor()
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};
var aboutimgs_ja_JP = {
    addNew: function () {
        $.get('/Admin/AboutAdmin/AboutImgs?language=4', function (template) {
            $('#js-aboutimgs4').append(template);
            addPartialViewValid();
            initCKeditor()
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};
var aboutimgs_ko_KR = {
    addNew: function () {
        $.get('/Admin/AboutAdmin/AboutImgs?language=5', function (template) {
            $('#js-aboutimgs5').append(template);
            addPartialViewValid();
            initCKeditor()
        });
    },
    remove: function (e) {
        const $this = $(e);
        $this.parent().parent().parent().remove();
    }
};

function addPartialViewValid() {
    $("form").each(function () { $.data($(this)[0], 'validator', false); });
    $.validator.unobtrusive.parse("form");
}
var option = {
    customConfig: '/Content/Admin/ckeditor/ckeditor_custom_config.js',
};
function initCKeditor() {
    $(".js-ckeditor").each(function () {
        var id = $(this).attr('id');
        CKEDITOR.replace(id, option);
    });
}
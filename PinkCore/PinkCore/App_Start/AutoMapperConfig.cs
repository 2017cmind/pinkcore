﻿using AutoMapper;
using PinkCore.Models;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台

                cfg.CreateMap<RelNewsNewsTranslations, Areas.Admin.ViewModels.News.NewsView>();
                cfg.CreateMap<Areas.Admin.ViewModels.News.NewsTranslationView, NewsTranslations>();
                cfg.CreateMap<NewsTranslations, Areas.Admin.ViewModels.News.NewsTranslationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.News.NewsView, News>();
                cfg.CreateMap<News, Areas.Admin.ViewModels.News.NewsView>();

                cfg.CreateMap<Areas.Admin.ViewModels.Banner.BannerIndexItemView, RelBannerBannerTranslations>();
                cfg.CreateMap<RelBannerBannerTranslations, Areas.Admin.ViewModels.Banner.BannerIndexItemView>();
                cfg.CreateMap<RelBannerBannerTranslations, Areas.Admin.ViewModels.Banner.BannerView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Banner.BannerTranslationView, BannerTranslations>();
                cfg.CreateMap<BannerTranslations, Areas.Admin.ViewModels.Banner.BannerTranslationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Banner.BannerView, Banner>();
                cfg.CreateMap<Banner, Areas.Admin.ViewModels.Banner.BannerView>();

                cfg.CreateMap<RelPolicyPolicyTranslations, Areas.Admin.ViewModels.Policy.PolicyView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Policy.PolicyTranslationView, PolicyTranslations>();
                cfg.CreateMap<PolicyTranslations, Areas.Admin.ViewModels.Policy.PolicyTranslationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Policy.PolicyView, Policy>();
                cfg.CreateMap<Policy, Areas.Admin.ViewModels.Policy.PolicyView>();

                cfg.CreateMap<RelAgeConfirmAgeConfirmTranslations, Areas.Admin.ViewModels.AgeConfirm.AgeConfirmView>();
                cfg.CreateMap<Areas.Admin.ViewModels.AgeConfirm.AgeConfirmTranslationView, AgeConfirmTranslations>();
                cfg.CreateMap<AgeConfirmTranslations, Areas.Admin.ViewModels.AgeConfirm.AgeConfirmTranslationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.AgeConfirm.AgeConfirmView, AgeConfirm>();
                cfg.CreateMap<AgeConfirm, Areas.Admin.ViewModels.AgeConfirm.AgeConfirmView>();

                cfg.CreateMap<Areas.Admin.ViewModels.Product.ProductIndexItemView, RelProductProductTranslations>();
                cfg.CreateMap<RelProductProductTranslations, Areas.Admin.ViewModels.Product.ProductIndexItemView>();
                cfg.CreateMap<RelProductProductTranslations, Areas.Admin.ViewModels.Product.ProductView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Product.ProductTranslationView, ProductTranslations>();
                cfg.CreateMap<ProductTranslations, Areas.Admin.ViewModels.Product.ProductTranslationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Product.ProductView, Product>();
                cfg.CreateMap<Product, Areas.Admin.ViewModels.Product.ProductView>();

                cfg.CreateMap<RelAboutAboutTranslations, Areas.Admin.ViewModels.About.AboutView>();
                cfg.CreateMap<Areas.Admin.ViewModels.About.AboutTranslationView, AboutTranslations>();
                cfg.CreateMap<AboutTranslations, Areas.Admin.ViewModels.About.AboutTranslationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.About.AboutView, About>();
                cfg.CreateMap<About, Areas.Admin.ViewModels.About.AboutView>();
                cfg.CreateMap<Areas.Admin.ViewModels.About.AboutImgView, AboutImg>();
                cfg.CreateMap<AboutImg, Areas.Admin.ViewModels.About.AboutImgView>();


                cfg.CreateMap<RelTopicTopicTranslations, Areas.Admin.ViewModels.Topic.TopicView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Topic.TopicTranslationView, TopicTranslations>();
                cfg.CreateMap<TopicTranslations, Areas.Admin.ViewModels.Topic.TopicTranslationView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Topic.TopicView, Topic>();
                cfg.CreateMap<Topic, Areas.Admin.ViewModels.Topic.TopicView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Topic.TopicImgView, TopicImg>();
                cfg.CreateMap<TopicImg, Areas.Admin.ViewModels.Topic.TopicImgView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Topic.TopicInformationsView, TopicInformation>();
                cfg.CreateMap<TopicInformation, Areas.Admin.ViewModels.Topic.TopicInformationsView>();
                #endregion
                #region 前台
                cfg.CreateMap<TopicInformation, ViewModels.Topic.TopicInformationsView>();
                cfg.CreateMap<TopicImg, ViewModels.Topic.TopicImgView>();

                cfg.CreateMap<RelNewsNewsTranslations, Areas.Darakugear.ViewModels.News.NewsItem>();

                cfg.CreateMap<RelTopicTopicTranslations, ViewModels.Topic.TopicView>();
                cfg.CreateMap<RelTopicTopicTranslations, ViewModels.Topic.TopicItem>();

                cfg.CreateMap<RelAboutAboutTranslations, ViewModels.About.AboutView>();
                cfg.CreateMap<RelPolicyPolicyTranslations, ViewModels.Policy.PolicyView>();
                cfg.CreateMap<RelBannerBannerTranslations, ViewModels.Home.BannerItem>();


                #endregion
            });
        }
    }
}
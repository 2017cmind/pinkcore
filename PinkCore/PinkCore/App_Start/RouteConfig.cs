﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PinkCore
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Topic",
                url: "{lang}/Topic/Detail/{id}",
                defaults: new { controller = "Topic", action = "Detail", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Default",
               "{lang}/{controller}/{action}/{id}",
               new { controller = "Home", action = "Index", id = UrlParameter.Optional, lang = "zh-TW" },
               namespaces: new[] { "PinkCore.Controllers" }
              );
        }
    }
}

﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.Product;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Repositories
{
    public class ProductRepository
    {
        private PinkCoreDBEntities db = new PinkCoreDBEntities();

        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="productTranslationsTw">
        /// 繁中
        /// </param>
        /// <param name="productTranslationsCn">
        /// 簡中
        /// </param>
        /// <param name="productTranslationsUs">
        /// 英文
        /// </param>
        /// <param name="productTranslationsJp">
        /// 日文
        /// </param>
        /// <param name="productTranslationsKr">
        /// 韓文
        /// </param>
        public int Insert(Product product, List<ProductTranslations> productTranslations)
        {
            DateTime now = DateTime.Now;
            product.CreateTime = now;
            product.UpdateTime = now;
            db.Product.Add(product);
            db.SaveChanges();

            int productId = product.ID;

            foreach (var item in productTranslations)
            {
                item.ProductId = productId;
                db.ProductTranslations.Add(item);
            }
            db.SaveChanges();

            return productId;
        }

        public void Update(Product newData, List<ProductTranslations> list)
        {
            DateTime now = DateTime.Now;

            Product oldData = db.Product.Find(newData.ID);
            oldData.Image = newData.Image;
            oldData.HoverImage = newData.HoverImage;
            oldData.IsLimit = newData.IsLimit;
            oldData.isHot = newData.isHot;
            oldData.OnlineTime = newData.OnlineTime;
            oldData.Type = newData.Type;
            oldData.Status = newData.Status;
            oldData.Sort = newData.Sort;
            oldData.SupportLan = newData.SupportLan;
            oldData.UpdateTime = now;

            foreach (var item in list)
            {
                ProductTranslations translations = db.ProductTranslations.Find(item.ID);
                translations.Description = item.Description;
                translations.Name = item.Name;
                translations.Link = item.Link;
                translations.Image = item.Image;
                translations.HoverImage = item.HoverImage;
            }

            db.SaveChanges();
        }

        public IQueryable<RelProductProductTranslations> GetAll()
        {
            var query = from n in db.Product
                        join t in db.ProductTranslations on n.ID equals t.ProductId
                        select new RelProductProductTranslations()
                        {
                            ID = n.ID,
                            Image = n.Image,
                            isHot = n.isHot,
                            HoverImage = n.HoverImage,
                            OnlineTime = n.OnlineTime,
                            IsLimit = n.IsLimit,
                            Type = n.Type,
                            Sort = n.Sort,
                            SupportLan = n.SupportLan,
                            Status = n.Status,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            Description = t.Description,
                            TranslationsImage = t.Image,
                            TranslationsHoverImage = t.HoverImage,
                            Link = t.Link,
                            Language = t.Language,
                            Name = t.Name,
                            ProductTranslationsID = t.ID
                        };
            return query;
        }

        public IQueryable<RelProductProductTranslations> Query(string description, int type, int language, bool? status,string category =null)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(description))
            {
                query = query.Where(p => p.Description.Contains(description));
            }
            if (type != 0)
            {
                query = query.Where(p => p.Type == type);
            }
            if (status.HasValue)
            {
                query = query.Where(p => p.Status == status);
            }
            if (language != 0)
            {
                query = query.Where(p => p.Language == language);
            }
            if (category == "women")
            {
                query = query.Where(p => p.IsLimit == false);
            }
            return query;
        }

        public ProductView FindBy(int id)
        {
            Product data = db.Product.Find(id);
            var result = Mapper.Map<ProductView>(data);
            result.OnlineTime = data.OnlineTime.ToString("yyyy/MM/dd");
            var productTranslationViewTw = db.ProductTranslations.Single(p => p.ProductId == id && p.Language == (int)Language.zh_TW);
            result.ProductTranslationViewTw = Mapper.Map<ProductTranslationView>(productTranslationViewTw);

            var productTranslationViewCn = db.ProductTranslations.Single(p => p.ProductId == id && p.Language == (int)Language.zh_CN);
            result.ProductTranslationViewCn = Mapper.Map<ProductTranslationView>(productTranslationViewCn);

            var productTranslationViewUs = db.ProductTranslations.Single(p => p.ProductId == id && p.Language == (int)Language.en_US);
            result.ProductTranslationViewUs = Mapper.Map<ProductTranslationView>(productTranslationViewUs);

            var productTranslationViewJp = db.ProductTranslations.Single(p => p.ProductId == id && p.Language == (int)Language.ja_JP);
            result.ProductTranslationViewJp = Mapper.Map<ProductTranslationView>(productTranslationViewJp);

            var productTranslationViewKr = db.ProductTranslations.Single(p => p.ProductId == id && p.Language == (int)Language.ko_KR);
            result.ProductTranslationViewKr = Mapper.Map<ProductTranslationView>(productTranslationViewKr);

            return result;
        }

        public string DeleteImage(int id, int type)
        {
            Product data = db.Product.Find(id);
            var image = "";
            if (type == (int)DeviceType.PC)
            {
                image = data.Image;
                data.Image = string.Empty;

            }
            else {
                image = data.HoverImage;
                data.HoverImage = string.Empty;
            }
            db.SaveChanges();

            return image;
        }
        public string DeleteImage(int id, int type, int language)
        {
            ProductTranslations data = db.ProductTranslations.Single(p => p.ProductId == id && p.Language == language);
            var image = "";
            if (type == (int)DeviceType.PC)
            {
                image = data.Image;
                data.Image = string.Empty;
            }
            else {
                image = data.HoverImage;
                data.HoverImage = string.Empty;
            }

            db.SaveChanges();

            return image;
        }

        public List<string> DeleteImages(int id)
        {
            var images = db.ProductTranslations.Where(p => p.ProductId == id).Select(p => p.Image).ToList();
            return images;
        }
        public List<string> DeleteHoverImages(int id)
        {
            var images = db.ProductTranslations.Where(p => p.ProductId == id).Select(p => p.HoverImage).ToList();
            return images;
        }

        public void Delete(int id)
        {
            Product product = db.Product.Find(id);
            db.Product.Remove(product);

            IEnumerable<ProductTranslations> productTranslations = db.ProductTranslations.Where(p => p.ProductId == id);
            db.ProductTranslations.RemoveRange(productTranslations);

            db.SaveChanges();
        }
        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RelProductProductTranslations Detail(int productId, Language language, string category)
        {
            var list = this.List(language, null);
            var query = list.Where(p => p.ID == productId).SingleOrDefault();
            return query;

        }

        public RelProductProductTranslations Preview(int productId, Language language)
        {
            var list = Query("", 0, (int)language, null, null);
            if (list.Any()) {
                var query = list.Single(p => p.ID == productId);
                return query;
            }

            return null;
        }

        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public IQueryable<RelProductProductTranslations> List(Language language, string category)
        {
            return Query("", 0, (int)language, true, category).OrderBy(p => p.Sort);
        }
        #endregion
    }
}
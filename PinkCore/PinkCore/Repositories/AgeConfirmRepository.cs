﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.AgeConfirm;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Repositories
{
    public class AgeConfirmRepository
    {
        private PinkCoreDBEntities db = new PinkCoreDBEntities();

        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="AgeConfirm"></param>
        /// <param name="AgeConfirmTranslationsTw">
        /// 繁中
        /// </param>
        /// <param name="AgeConfirmTranslationsCn">
        /// 簡中
        /// </param>
        /// <param name="AgeConfirmTranslationsUs">
        /// 英文
        /// </param>
        /// <param name="AgeConfirmTranslationsJp">
        /// 日文
        /// </param>
        /// <param name="AgeConfirmTranslationsKr">
        /// 韓文
        /// </param>
        public int Insert(AgeConfirm AgeConfirm, List<AgeConfirmTranslations> AgeConfirmTranslations)
        {
            DateTime now = DateTime.Now;
            AgeConfirm.CreateTime = now;
            AgeConfirm.UpdateTime = now;
            db.AgeConfirm.Add(AgeConfirm);
            db.SaveChanges();

            int AgeConfirmId = AgeConfirm.ID;

            foreach (var item in AgeConfirmTranslations)
            {
                item.AgeConfirmId = AgeConfirmId;
                db.AgeConfirmTranslations.Add(item);
            }
            db.SaveChanges();

            return AgeConfirmId;
        }

        public void Update(AgeConfirm newData, List<AgeConfirmTranslations> list)
        {
            DateTime now = DateTime.Now;

            AgeConfirm oldData = db.AgeConfirm.Find(newData.ID);
            oldData.PCImage = newData.PCImage;
            oldData.MoblieImage = newData.MoblieImage;
            oldData.UpdateTime = now;

            foreach (var item in list)
            {
                AgeConfirmTranslations dataTranslations = db.AgeConfirmTranslations.Find(item.ID);
                dataTranslations.PCImage = item.PCImage;
                dataTranslations.MoblieImage = item.MoblieImage;
            }

            db.SaveChanges();
        }

        public IQueryable<RelAgeConfirmAgeConfirmTranslations> GetAll()
        {
            var query = from n in db.AgeConfirm
                        join t in db.AgeConfirmTranslations on n.ID equals t.AgeConfirmId
                        select new RelAgeConfirmAgeConfirmTranslations()
                        {
                            ID = n.ID,
                            PCImage = n.PCImage,
                            MoblieImage = n.MoblieImage,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            AgeConfirmTranslationsID = t.ID,
                            Language = t.Language,
                            TranslationsPCImage = t.PCImage,
                            TranslationsMoblieImage = t.MoblieImage,
                        };
            return query;
        }

        public IQueryable<RelAgeConfirmAgeConfirmTranslations> Query( int language)
        {
            var query = GetAll();
            if (language != 0)
            {
                query = query.Where(p => p.Language == language);
            }
            return query;
        }

        public AgeConfirmView FirstData()
        {
            AgeConfirm data = db.AgeConfirm.FirstOrDefault();
            var id = data.ID;
            var result = Mapper.Map<AgeConfirmView>(data);
            var AgeConfirmTranslationViewTw = db.AgeConfirmTranslations.Single(p => p.AgeConfirmId == id && p.Language == (int)Language.zh_TW);
            result.AgeConfirmTranslationViewTw = Mapper.Map<AgeConfirmTranslationView>(AgeConfirmTranslationViewTw);

            var AgeConfirmTranslationViewCn = db.AgeConfirmTranslations.Single(p => p.AgeConfirmId == id && p.Language == (int)Language.zh_CN);
            result.AgeConfirmTranslationViewCn = Mapper.Map<AgeConfirmTranslationView>(AgeConfirmTranslationViewCn);

            var AgeConfirmTranslationViewUs = db.AgeConfirmTranslations.Single(p => p.AgeConfirmId == id && p.Language == (int)Language.en_US);
            result.AgeConfirmTranslationViewUs = Mapper.Map<AgeConfirmTranslationView>(AgeConfirmTranslationViewUs);

            var AgeConfirmTranslationViewJp = db.AgeConfirmTranslations.Single(p => p.AgeConfirmId == id && p.Language == (int)Language.ja_JP);
            result.AgeConfirmTranslationViewJp = Mapper.Map<AgeConfirmTranslationView>(AgeConfirmTranslationViewJp);

            var AgeConfirmTranslationViewKr = db.AgeConfirmTranslations.Single(p => p.AgeConfirmId == id && p.Language == (int)Language.ko_KR);
            result.AgeConfirmTranslationViewKr = Mapper.Map<AgeConfirmTranslationView>(AgeConfirmTranslationViewKr);

            return result;
        }

        public void Delete(int id)
        {
            AgeConfirm AgeConfirm = db.AgeConfirm.Find(id);
            db.AgeConfirm.Remove(AgeConfirm);

            IEnumerable<AgeConfirmTranslations> AgeConfirmTranslations = db.AgeConfirmTranslations.Where(p => p.AgeConfirmId == id);
            db.AgeConfirmTranslations.RemoveRange(AgeConfirmTranslations);

            db.SaveChanges();
        }

        public string DeleteImage(int id, int type)
        {
            AgeConfirm data = db.AgeConfirm.Find(id);
            var image = "";
            switch (type)
            {
                case (int)DeviceType.PC:
                    image = data.PCImage;
                    data.PCImage = string.Empty;
                    break;
                case (int)DeviceType.Moblie:
                    image = data.MoblieImage;
                    data.MoblieImage = string.Empty;
                    break;
            }
            db.SaveChanges();

            return image;
        }
        public string DeleteImage(int id, int type, int language)
        {
            AgeConfirmTranslations data = db.AgeConfirmTranslations.Single(p => p.AgeConfirmId == id && p.Language == language);
            var image ="";
            switch (type)
            {
                case (int)DeviceType.PC:
                    image = data.PCImage;
                    data.PCImage = string.Empty;
                    break;
                case (int)DeviceType.Moblie:
                    image = data.MoblieImage;
                    data.MoblieImage = string.Empty;
                    break;
            }
            db.SaveChanges();

            return image;
        }

        public List<string> DeletePCImages(int id)
        {
            var images = db.AgeConfirmTranslations.Where(p => p.AgeConfirmId == id).Select(p => p.PCImage).ToList();
            return images;
        }

        public List<string> DeleteMoblieImages(int id)
        {
            var images = db.AgeConfirmTranslations.Where(p => p.AgeConfirmId == id).Select(p => p.MoblieImage).ToList();
            return images;
        }
        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RelAgeConfirmAgeConfirmTranslations Detail(Language language)
        {
            var list = GetAll().Where(p => p.Language == (int)language);
            var query = list.Single();

            return query;
        }

        #endregion
    }
}
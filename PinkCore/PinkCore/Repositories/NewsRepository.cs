﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.News;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Repositories
{
    public class NewsRepository
    {
        private PinkCoreDBEntities db = new PinkCoreDBEntities();
        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="news"></param>
        /// <param name="newsTranslationsTw">
        /// 繁中
        /// </param>
        /// <param name="newsTranslationsCn">
        /// 簡中
        /// </param>
        /// <param name="newsTranslationsUs">
        /// 英文
        /// </param>
        /// <param name="newsTranslationsJp">
        /// 日文
        /// </param>
        /// <param name="newsTranslationsKr">
        /// 韓文
        /// </param>
        public int Insert(News news, List<NewsTranslations> newsTranslations)
        {
            DateTime now = DateTime.Now;
            news.CreateTime = now;
            news.UpdateTime = now;
            db.News.Add(news);
            db.SaveChanges();

            int newsId = news.ID;

            foreach (var item in newsTranslations)
            {
                item.NewsId = newsId;
                db.NewsTranslations.Add(item);
            }
            db.SaveChanges();

            return newsId;
        }

        public void Update(News newData, List<NewsTranslations> list)
        {
            DateTime now = DateTime.Now;

            News oldData = db.News.Find(newData.ID);
            oldData.Image = newData.Image;
            oldData.ProductId = newData.ProductId;
            oldData.Status = newData.Status;
            oldData.OnlineTime = newData.OnlineTime;
            oldData.UpdateTime = now;

            foreach (var item in list)
            {
                NewsTranslations translations = db.NewsTranslations.Find(item.ID);
                translations.Image = item.Image;
                translations.Tags = item.Tags;
                translations.Title = item.Title;
                translations.Content = item.Content;
                translations.LinkUrl = item.LinkUrl;
            }

            db.SaveChanges();
        }

        public NewsTranslations FindIdByLinkTitle(string title, int lang)
        {
            var query = db.NewsTranslations.Where(p => p.LinkUrl == title && p.Language == lang);
            if (query.Any())
            {
                return query.Single();
            }
            return null;
        }

        public NewsTranslations FindTopicIdByLinkTitle(string title)
        {
            var query = db.NewsTranslations.Where(p => p.LinkUrl == title);
            return query.Single();
        }

        public IQueryable<RelNewsNewsTranslations> GetAll()
        {
            var query = from n in db.News
                        join t in db.NewsTranslations on n.ID equals t.NewsId
                        select new RelNewsNewsTranslations()
                        {
                            ID = n.ID,
                            Image = n.Image,
                            OnlineTime= n.OnlineTime,
                            ProductId = n.ProductId,
                            Status = n.Status,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            Content = t.Content,
                            TranslationsImage = t.Image,
                            Tags = t.Tags,
                            LinkUrl = t.LinkUrl,
                            Title = t.Title,
                            Language = t.Language,
                            NewsTranslationsID = t.ID,
                        };
            return query;
        }

        public IQueryable<RelNewsNewsTranslations> Query(string title, int productId, int language, bool? status)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            if (productId != 0)
            {
                query = query.Where(p => p.ProductId == productId);
            }
            if (status.HasValue)
            {
                query = query.Where(p => p.Status == status);
            }
            if (language != 0)
            {
                query = query.Where(p => p.Language == language);
            }
            
            return query;
        }

        public NewsView FindBy(int id)
        {
            News data = db.News.Find(id);
            var result = Mapper.Map<NewsView>(data);
            result.OnlineTime = data.OnlineTime.ToString("yyyy/MM/dd HH:mm");
            var newsTranslationViewTw = db.NewsTranslations.Single(p => p.NewsId == id && p.Language == (int)Language.zh_TW);
            result.NewsTranslationViewTw = Mapper.Map<NewsTranslationView>(newsTranslationViewTw);

            var newsTranslationViewCn = db.NewsTranslations.Single(p => p.NewsId == id && p.Language == (int)Language.zh_CN);
            result.NewsTranslationViewCn = Mapper.Map<NewsTranslationView>(newsTranslationViewCn);

            var newsTranslationViewUs = db.NewsTranslations.Single(p => p.NewsId == id && p.Language == (int)Language.en_US);
            result.NewsTranslationViewUs = Mapper.Map<NewsTranslationView>(newsTranslationViewUs);

            var newsTranslationViewJp = db.NewsTranslations.Single(p => p.NewsId == id && p.Language == (int)Language.ja_JP);
            result.NewsTranslationViewJp = Mapper.Map<NewsTranslationView>(newsTranslationViewJp);

            var newsTranslationViewKr = db.NewsTranslations.Single(p => p.NewsId == id && p.Language == (int)Language.ko_KR);
            result.NewsTranslationViewKr = Mapper.Map<NewsTranslationView>(newsTranslationViewKr);

            return result;
        }

        public string DeleteImage(int id)
        {
            News data = db.News.Find(id);
            var image = data.Image;
            data.Image = string.Empty;

            db.SaveChanges();

            return image;
        }
        public string DeleteImage(int id, int language)
        {
            NewsTranslations data = db.NewsTranslations.Single(p => p.NewsId == id && p.Language == language);
            var image = data.Image;
            data.Image = string.Empty;        
            db.SaveChanges();

            return image;
        }

        public List<string> DeleteImages(int id)
        {
            var images = db.NewsTranslations.Where(p => p.NewsId == id).Select(p => p.Image).ToList();
            return images;
        }

        public void Delete(int id)
        {
            News news = db.News.Find(id);
            db.News.Remove(news);

            IEnumerable<NewsTranslations> newsTranslations = db.NewsTranslations.Where(p => p.NewsId == id);
            db.NewsTranslations.RemoveRange(newsTranslations);

            db.SaveChanges();
        }

        public NewsTranslations FindIdByTitle(string title)
        {
            var query = db.NewsTranslations.Where(p => p.Title == title);
            if (query.Count() == 1)
            {
                return query.Single();
            }
            if (query.Count() >= 2)
            {
                return query.FirstOrDefault();
            }
            return null;
        }
        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RelNewsNewsTranslations Detail(int newsId, Language language, int productId)
        {
            var list = this.List(language, productId);
            if (list.Any())
            {
                var query = list.Single(p => p.ID == newsId);
                return query;
            }
            else {
                return null;
            }

        }

        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public IQueryable<RelNewsNewsTranslations> List(Language language, int productId)
        {
            var query = Query("", productId, (int)language, true);
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime dtCurrentTime = TimeZoneInfo.ConvertTime(DateTime.Now, tzi);
            return query.Where(p => p.OnlineTime <= dtCurrentTime).OrderByDescending(p => p.OnlineTime);
        }
        #endregion
    }
}
﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.Banner;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Repositories
{
    public class BannerRepository
    {
        private PinkCoreDBEntities db = new PinkCoreDBEntities();

        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="banner"></param>
        /// <param name="bannerTranslationsTw">
        /// 繁中
        /// </param>
        /// <param name="bannerTranslationsCn">
        /// 簡中
        /// </param>
        /// <param name="bannerTranslationsUs">
        /// 英文
        /// </param>
        /// <param name="bannerTranslationsJp">
        /// 日文
        /// </param>
        /// <param name="bannerTranslationsKr">
        /// 韓文
        /// </param>
        public int Insert(Banner banner, List<BannerTranslations> bannerTranslations)
        {
            DateTime now = DateTime.Now;
            banner.CreateTime = now;
            banner.UpdateTime = now;
            db.Banner.Add(banner);
            db.SaveChanges();

            int bannerId = banner.ID;

            foreach (var item in bannerTranslations)
            {
                item.BannerId = bannerId;
                db.BannerTranslations.Add(item);
            }
            db.SaveChanges();

            return bannerId;
        }

        public void Update(Banner newData, List<BannerTranslations> list)
        {
            DateTime now = DateTime.Now;

            Banner oldData = db.Banner.Find(newData.ID);
            oldData.Icon = newData.Icon;
            oldData.IsLimit = newData.IsLimit;
            oldData.HoverImage = newData.HoverImage;
            oldData.PCImage = newData.PCImage;
            oldData.MoblieImage = newData.MoblieImage;
            oldData.Status = newData.Status;
            oldData.Sort = newData.Sort;
            oldData.UpdateTime = now;

            foreach (var item in list)
            {
                BannerTranslations translations = db.BannerTranslations.Find(item.ID);
                translations.Description = item.Description;
                translations.HoverImage = item.HoverImage;
                translations.MoblieImage = item.MoblieImage;
                translations.PCImage = item.PCImage;
                translations.Link = item.Link;
            }

            db.SaveChanges();
        }

        public IQueryable<RelBannerBannerTranslations> GetAll()
        {
            var query = from n in db.Banner
                        join t in db.BannerTranslations on n.ID equals t.BannerId
                        select new RelBannerBannerTranslations()
                        {
                            ID = n.ID,
                            PCImage = n.PCImage,
                            MoblieImage = n.MoblieImage,
                            isLimit = n.IsLimit,
                            HoverImage = n.HoverImage,
                            Icon = n.Icon,
                            Sort = n.Sort,
                            Status = n.Status,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            Description = t.Description,
                            TranslationsHoverImage = t.HoverImage,
                            TranslationsMoblieImage = t.MoblieImage,
                            TranslationsPCImage = t.PCImage,
                            Link = t.Link,
                            Language = t.Language,
                            BannerTranslationsID = t.ID,
                        };
            return query;
        }

        public IQueryable<RelBannerBannerTranslations> Query(string description, int language, bool? status, string category = null)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(description))
            {
                query = query.Where(p => p.Description.Contains(description));
            }
            if (status.HasValue)
            {
                query = query.Where(p => p.Status == status);
            }
            if (language != 0)
            {
                query = query.Where(p => p.Language == language);
            }
            //1.all全部都看連限制級都可看，2.women只喜歡看帥哥不會有限制級
            if (category == "women")
            {
                query = query.Where(p => p.isLimit == false);
            }
            return query;
        }

        public BannerView FindBy(int id)
        {
            Banner data = db.Banner.Find(id);
            var result = Mapper.Map<BannerView>(data);
            var bannerTranslationViewTw = db.BannerTranslations.Single(p => p.BannerId == id && p.Language == (int)Language.zh_TW);
            result.BannerTranslationViewTw = Mapper.Map<BannerTranslationView>(bannerTranslationViewTw);

            var bannerTranslationViewCn = db.BannerTranslations.Single(p => p.BannerId == id && p.Language == (int)Language.zh_CN);
            result.BannerTranslationViewCn = Mapper.Map<BannerTranslationView>(bannerTranslationViewCn);

            var bannerTranslationViewUs = db.BannerTranslations.Single(p => p.BannerId == id && p.Language == (int)Language.en_US);
            result.BannerTranslationViewUs = Mapper.Map<BannerTranslationView>(bannerTranslationViewUs);

            var bannerTranslationViewJp = db.BannerTranslations.Single(p => p.BannerId == id && p.Language == (int)Language.ja_JP);
            result.BannerTranslationViewJp = Mapper.Map<BannerTranslationView>(bannerTranslationViewJp);

            var bannerTranslationViewKr = db.BannerTranslations.Single(p => p.BannerId == id && p.Language == (int)Language.ko_KR);
            result.BannerTranslationViewKr = Mapper.Map<BannerTranslationView>(bannerTranslationViewKr);

            return result;
        }

        public string DeleteImage(int id, int type)
        {
            Banner data = db.Banner.Find(id);
            var image = "";
            switch (type)
            {
                case (int)DeviceType.PC:
                    image = data.PCImage;
                    data.PCImage = string.Empty;
                    break;
                case (int)DeviceType.Moblie:
                    image = data.MoblieImage;
                    data.MoblieImage = string.Empty;
                    break;
                case (int)DeviceType.PCHover:
                    image = data.HoverImage;
                    data.HoverImage = string.Empty;
                    break;
                case 0:
                    image = data.Icon;
                    data.Icon = string.Empty;
                    break;
            }
            db.SaveChanges();

            return image;
        }
        public string DeleteImage(int id, int type, int language)
        {
            BannerTranslations data = db.BannerTranslations.Single(p => p.BannerId == id && p.Language == language);
            var image = "";
            switch (type)
            {
                case (int)DeviceType.PC:
                    image = data.PCImage;
                    data.PCImage = string.Empty;
                    break;
                case (int)DeviceType.Moblie:
                    image = data.MoblieImage;
                    data.MoblieImage = string.Empty;
                    break;
                case (int)DeviceType.PCHover:
                    image = data.HoverImage;
                    data.HoverImage = string.Empty;
                    break;
            }
            db.SaveChanges();

            return image;
        }

        public List<string> DeletePCImages(int id)
        {
            var images = db.BannerTranslations.Where(p => p.BannerId == id).Select(p => p.PCImage).ToList();
            return images;
        }

        public List<string> DeleteMoblieImages(int id)
        {
            var images = db.BannerTranslations.Where(p => p.BannerId == id).Select(p => p.MoblieImage).ToList();
            return images;
        }

        public List<string> DeleteHoverImages(int id)
        {
            var images = db.BannerTranslations.Where(p => p.BannerId == id).Select(p => p.HoverImage).ToList();
            return images;
        }

        public void Delete(int id)
        {
            Banner banner = db.Banner.Find(id);
            db.Banner.Remove(banner);

            IEnumerable<BannerTranslations> bannerTranslations = db.BannerTranslations.Where(p => p.BannerId == id);
            db.BannerTranslations.RemoveRange(bannerTranslations);

            db.SaveChanges();
        }
        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RelBannerBannerTranslations Detail(int bannerId, Language language)
        {
            var list = this.List(language, null);
            if (list.Any())
            {
                var query = list.Single(p => p.ID == bannerId);
                return query;
            }
            else {
                return null;
            }

        }

        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public IQueryable<RelBannerBannerTranslations> List(Language language, string category)
        {
            return Query("", (int)language, true, category).OrderBy(p => p.Sort);
        }
        #endregion
    }
}
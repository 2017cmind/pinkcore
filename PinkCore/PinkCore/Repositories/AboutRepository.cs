﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.About;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Repositories
{
    public class AboutRepository
    {
        private PinkCoreDBEntities db = new PinkCoreDBEntities();

        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="about"></param>
        /// <param name="aboutTranslationsTw">
        /// 繁中
        /// </param>
        /// <param name="aboutTranslationsCn">
        /// 簡中
        /// </param>
        /// <param name="aboutTranslationsUs">
        /// 英文
        /// </param>
        /// <param name="aboutTranslationsJp">
        /// 日文
        /// </param>
        /// <param name="aboutTranslationsKr">
        /// 韓文
        /// </param>
        public int Insert(About about, List<AboutTranslations> aboutTranslations)
        {
            DateTime now = DateTime.Now;
            about.CreateTime = now;
            about.UpdateTime = now;
            db.About.Add(about);
            db.SaveChanges();

            int aboutId = about.ID;

            foreach (var item in aboutTranslations)
            {
                item.AboutId = aboutId;
                db.AboutTranslations.Add(item);
            }
            db.SaveChanges();

            return aboutId;
        }

        public void Update(About newData, List<AboutTranslations> list, List<AboutImg> aboutTW, List<AboutImg> aboutCN, List<AboutImg> aboutEN, List<AboutImg> aboutKR, List<AboutImg> aboutJP)
        {
            DateTime now = DateTime.Now;

            About oldData = db.About.Find(newData.ID);
            oldData.ContactImg = newData.ContactImg;
            oldData.BannerImg = newData.BannerImg;
            oldData.AboutImg = newData.AboutImg;
            oldData.UpdateTime = now;

            foreach (var item in list)
            {
                AboutTranslations translations = db.AboutTranslations.Find(item.ID);
                translations.ContactTitle = item.ContactTitle;
                translations.ContactContent = item.ContactContent;
                translations.AboutTitle = item.AboutTitle;
                translations.AboutContent = item.AboutContent;
                var removeitems = db.AboutImg.Where(p => p.AboutTranslationsId == item.ID);
                if (removeitems.Count() != 0)
                {
                    db.AboutImg.RemoveRange(removeitems);
                }
                if (translations.Language == (int)Language.zh_TW) {
                    foreach (var img in aboutTW)
                    {
                        img.AboutTranslationsId = item.ID;
                        db.AboutImg.Add(img);
                    }
                }
                if (translations.Language == (int)Language.en_US)
                {
                    foreach (var img in aboutEN)
                    {
                        img.AboutTranslationsId = item.ID;
                        db.AboutImg.Add(img);
                    }
                }
                if (translations.Language == (int)Language.zh_CN)
                {
                    foreach (var img in aboutCN)
                    {
                        img.AboutTranslationsId = item.ID;
                        db.AboutImg.Add(img);
                    }
                }
                if (translations.Language == (int)Language.ja_JP)
                {
                    foreach (var img in aboutJP)
                    {
                        img.AboutTranslationsId = item.ID;
                        db.AboutImg.Add(img);
                    }
                }
                if (translations.Language == (int)Language.ko_KR)
                {
                    foreach (var img in aboutKR)
                    {
                        img.AboutTranslationsId = item.ID;
                        db.AboutImg.Add(img);
                    }
                }
            }

            db.SaveChanges();
        }

        public IQueryable<RelAboutAboutTranslations> GetAll()
        {
            var query = from n in db.About
                        join t in db.AboutTranslations on n.ID equals t.AboutId
                        select new RelAboutAboutTranslations()
                        {
                            ID = n.ID,
                            AboutImg = n.AboutImg,
                            ContactImg = n.ContactImg,
                            BannerImg = n.BannerImg,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            ContactContent = t.ContactContent,
                            ContactTitle = t.ContactTitle,
                            AboutContent = t.AboutContent,
                            Language = t.Language,
                            AboutTitle = t.AboutTitle,
                            AboutTranslationsID = t.ID,
                            AboutImgs = db.AboutImg.Where(p => p.AboutTranslationsId == t.ID).ToList(),
                        };
            return query;
        }

        public IQueryable<RelAboutAboutTranslations> Query(int language)
        {
            var query = GetAll();
            if (language != 0)
            {
                query = query.Where(p => p.Language == language);
            }
            return query;
        }

        public AboutView FirstData()
        {
            About data = db.About.FirstOrDefault();
            var id = data.ID;
            var result = Mapper.Map<AboutView>(data);
            var AboutTranslationViewTw = db.AboutTranslations.Single(p => p.AboutId == id && p.Language == (int)Language.zh_TW);
            result.AboutTranslationViewTw = Mapper.Map<AboutTranslationView>(AboutTranslationViewTw);

            var AboutTranslationViewCn = db.AboutTranslations.Single(p => p.AboutId == id && p.Language == (int)Language.zh_CN);
            result.AboutTranslationViewCn = Mapper.Map<AboutTranslationView>(AboutTranslationViewCn);

            var AboutTranslationViewUs = db.AboutTranslations.Single(p => p.AboutId == id && p.Language == (int)Language.en_US);
            result.AboutTranslationViewUs = Mapper.Map<AboutTranslationView>(AboutTranslationViewUs);

            var AboutTranslationViewJp = db.AboutTranslations.Single(p => p.AboutId == id && p.Language == (int)Language.ja_JP);
            result.AboutTranslationViewJp = Mapper.Map<AboutTranslationView>(AboutTranslationViewJp);

            var AboutTranslationViewKr = db.AboutTranslations.Single(p => p.AboutId == id && p.Language == (int)Language.ko_KR);
            result.AboutTranslationViewKr = Mapper.Map<AboutTranslationView>(AboutTranslationViewKr);

            return result;
        }


        public IQueryable<AboutImg> GetImgs(int aboutTranslationTwId, int aboutTranslationCnId, int aboutTranslationUsId, int aboutTranslationKrId, int aboutTranslationJpId)
        {
            var data = db.AboutImg.AsQueryable();

            if (aboutTranslationTwId != 0) {
                data = db.AboutImg.Where(p => p.AboutTranslationsId == aboutTranslationTwId);
            }
            if (aboutTranslationCnId != 0)
            {
                data = db.AboutImg.Where(p => p.AboutTranslationsId == aboutTranslationCnId);
            }
            if (aboutTranslationUsId != 0)
            {
                data = db.AboutImg.Where(p => p.AboutTranslationsId == aboutTranslationUsId);
            }
            if (aboutTranslationKrId != 0)
            {
                data = db.AboutImg.Where(p => p.AboutTranslationsId == aboutTranslationKrId);
            }
            if (aboutTranslationJpId != 0)
            {
                data = db.AboutImg.Where(p => p.AboutTranslationsId == aboutTranslationJpId);
            }
         
            //var result = Mapper.Map<IEnumerable<AboutImgView>()>(data);

            return data;
        }
        public string DeleteImage(int id, int type)
        {
            About data = db.About.Find(id);
            var image = "";
            //1.關於我們圖片 2.團隊輪播主圖 3.聯絡我們圖片
            switch (type)
            {
                case (int)AboutImgType.AboutImg:
                    image = data.AboutImg;
                    data.AboutImg = string.Empty;
                    break;
                case (int)AboutImgType.ContactImg:
                    image = data.ContactImg;
                    data.ContactImg = string.Empty;
                    break;
                case (int)AboutImgType.BannerImg:
                    image = data.BannerImg;
                    data.BannerImg = string.Empty;
                    break;
            }
            db.SaveChanges();

            return image;
        }
        public string DeleteAboutImages(string imagefile)
        {
            var data = db.AboutImg.Where(p => p.Image == imagefile).Single();
            var image = data.Image;
            data.Image = string.Empty;
            db.SaveChanges();
            return image;
        }

        public void Delete(int id)
        {
            About about = db.About.Find(id);
            db.About.Remove(about);

            IEnumerable<AboutTranslations> aboutTranslations = db.AboutTranslations.Where(p => p.AboutId == id);
            db.AboutTranslations.RemoveRange(aboutTranslations);

            db.SaveChanges();
        }
        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RelAboutAboutTranslations Detail(int aboutId, Language language)
        {
            var query = Query((int)language)?.FirstOrDefault();
            if (query.AboutImgs.Any()) {
                query.AboutImgs = query.AboutImgs.Where(p => p.Status == true).OrderBy(p =>p.Sort).ToList();
            }
            return query;
        }
        #endregion
    }
}
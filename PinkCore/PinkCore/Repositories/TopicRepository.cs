﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.Topic;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Repositories
{
    public class TopicRepository
    {
        private PinkCoreDBEntities db = new PinkCoreDBEntities();

        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="topicTranslationsTw">
        /// 繁中
        /// </param>
        /// <param name="topicTranslationsCn">
        /// 簡中
        /// </param>
        /// <param name="topicTranslationsUs">
        /// 英文
        /// </param>
        /// <param name="topicTranslationsJp">
        /// 日文
        /// </param>
        /// <param name="topicTranslationsKr">
        /// 韓文
        /// </param>
        public int Insert(Topic topic, List<TopicTranslations> topicTranslations)
        {
            DateTime now = DateTime.Now;
            topic.CreateTime = now;
            topic.UpdateTime = now;
            db.Topic.Add(topic);
            db.SaveChanges();

            int topicId = topic.ID;

            foreach (var item in topicTranslations)
            {
                item.TopicId = topicId;
                db.TopicTranslations.Add(item);
            }
            db.SaveChanges();

            return topicId;
        }

        public void InsertTopicInformation(TopicTranslations data, Topic topic)
        {
            DateTime now = DateTime.Now;
            Topic oldtopic = db.Topic.Find(data.TopicId);
            oldtopic.Updater = topic.Updater;
            oldtopic.UpdateTime = now;

            var removeitems = db.TopicInformations.Where(p => p.TopicTranslationsId == data.ID);
            if (removeitems.Count() != 0)
            {
                foreach (var item in removeitems)
                {
                    var topicImgs = db.TopicImgs.Where(p => p.TopicInformationId == item.ID);

                    db.TopicImgs.RemoveRange(topicImgs);
                }
                db.TopicInformations.RemoveRange(removeitems);
            }

            TopicTranslations oldData = db.TopicTranslations.Find(data.ID);
            oldData.TopicInformations = data.TopicInformations;

            db.SaveChanges();
        }

        public void Update(Topic newData, List<TopicTranslations> list)
        {
            DateTime now = DateTime.Now;

            Topic oldData = db.Topic.Find(newData.ID);
            oldData.Image = newData.Image;
            oldData.HoverImage = newData.HoverImage;
            oldData.Status = newData.Status;
            oldData.OnlineTime = newData.OnlineTime;
            oldData.IsLimit = newData.IsLimit;
            oldData.ProductId = newData.ProductId;
            oldData.UpdateTime = now;

            foreach (var item in list)
            {
                TopicTranslations translations = db.TopicTranslations.Find(item.ID);
                translations.Image = item.Image;
                translations.Tags = item.Tags;
                translations.Title = item.Title;
                translations.LinkUrl = item.LinkUrl;
                translations.HoverImage = item.HoverImage;
                var removeitems = db.TopicInformations.Where(p => p.TopicTranslationsId == translations.ID);
                if (removeitems.Count() != 0)
                {
                    foreach (var itemInformations in removeitems)
                    {
                        var topicImgs = db.TopicImgs.Where(p => p.TopicInformationId == itemInformations.ID);
                        db.TopicImgs.RemoveRange(topicImgs);
                    }
                    db.TopicInformations.RemoveRange(removeitems);
                }
                translations.TopicInformations = item.TopicInformations;
            }

            db.SaveChanges();
        }

        public IQueryable<RelTopicTopicTranslations> GetAll()
        {
            var query = from n in db.Topic
                        join t in db.TopicTranslations on n.ID equals t.TopicId
                        select new RelTopicTopicTranslations()
                        {
                            ID = n.ID,
                            Image = n.Image,
                            HoverImage = n.HoverImage,
                            OnlineTime = n.OnlineTime,
                            Status = n.Status,
                            IsLimit = n.IsLimit,
                            ProductId = n.ProductId,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            Title = t.Title,
                            LinkUrl = t.LinkUrl,
                            TranslationsHoverImage = t.HoverImage,
                            TranslationsImage = t.Image,
                            Language = t.Language,
                            Tags = t.Tags,
                            TopicInformations = t.TopicInformations.OrderBy(p => p.Sort).ToList(),
                            TopicTranslationsID = t.ID,
                        };
            return query;
        }

        public IQueryable<RelTopicTopicTranslations> Query(string title,int productId, bool ? status, int language)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(p => p.Title.Contains(title));
            }
            if (productId != 0) {
                query = query.Where(p => p.ProductId == productId);
            }
            if (status.HasValue) {
                query = query.Where(p => p.Status == status);
            }
            if (language != 0)
            {
                query = query.Where(p => p.Language == language);
            }
            return query;
        }

        public TopicTranslations FindIdByTitle(string title)
        {
           var query = db.TopicTranslations.Where(p => p.Title == title);
            if (query.Count() == 1) {
                return query.Single();
            }
            if (query.Count() >= 2)
            {
                return query.FirstOrDefault();
            }
            return null;
        }

        public TopicTranslations FindIdByLinkTitle(string title, int lang)
        {
            var query = db.TopicTranslations.Where(p => p.LinkUrl == title && p.Language == lang);
            if (query.Any())
            {
                return query.Single();
            }
            return null;
        }

        public TopicTranslations FindTopicIdByLinkTitle(string title)
        {
            var query = db.TopicTranslations.Where(p => p.LinkUrl == title);
            return query.Single();
        }

        public TopicView FindBy(int id)
        {
            Topic data = db.Topic.Find(id);
            var result = Mapper.Map<TopicView>(data);
            result.OnlineTime = data.OnlineTime.ToString("yyyy/MM/dd HH:mm");

            var topicTranslationViewTw = db.TopicTranslations.Single(p => p.TopicId == id && p.Language == (int)Language.zh_TW);
            topicTranslationViewTw.TopicInformations = topicTranslationViewTw.TopicInformations.OrderBy(p => p.Sort).ToList();
            result.TopicTranslationViewTw = Mapper.Map<TopicTranslationView>(topicTranslationViewTw);

            var topicTranslationViewCn = db.TopicTranslations.Single(p => p.TopicId == id && p.Language == (int)Language.zh_CN);
            topicTranslationViewCn.TopicInformations = topicTranslationViewCn.TopicInformations.OrderBy(p => p.Sort).ToList();
            result.TopicTranslationViewCn = Mapper.Map<TopicTranslationView>(topicTranslationViewCn);

            var topicTranslationViewUs = db.TopicTranslations.Single(p => p.TopicId == id && p.Language == (int)Language.en_US);
            topicTranslationViewUs.TopicInformations = topicTranslationViewUs.TopicInformations.OrderBy(p => p.Sort).ToList();
            result.TopicTranslationViewUs = Mapper.Map<TopicTranslationView>(topicTranslationViewUs);

            var topicTranslationViewJp = db.TopicTranslations.Single(p => p.TopicId == id && p.Language == (int)Language.ja_JP);
            topicTranslationViewJp.TopicInformations = topicTranslationViewJp.TopicInformations.OrderBy(p => p.Sort).ToList();
            result.TopicTranslationViewJp = Mapper.Map<TopicTranslationView>(topicTranslationViewJp);

            var topicTranslationViewKr = db.TopicTranslations.Single(p => p.TopicId == id && p.Language == (int)Language.ko_KR);
            topicTranslationViewKr.TopicInformations = topicTranslationViewKr.TopicInformations.OrderBy(p => p.Sort).ToList();
            result.TopicTranslationViewKr = Mapper.Map<TopicTranslationView>(topicTranslationViewKr);

            return result;
        }

        public TopicTranslations FindTopicInformation(int id)
        {
            TopicTranslations data = db.TopicTranslations.Find(id);
            return data;
        }

        public string DeleteImage(int id, int type)
        {
            Topic data = db.Topic.Find(id);
            var image = "";
            switch (type)
            {
                case (int)DeviceType.PC:
                    image = data.Image;
                    data.Image = string.Empty;
                    break;
                case (int)DeviceType.PCHover:
                    image = data.HoverImage;
                    data.HoverImage = string.Empty;
                    break;
            }
            db.SaveChanges();

            return image;
        }
        public string DeleteImage(int id, int type, int language)
        {
            TopicTranslations data = db.TopicTranslations.Single(p => p.TopicId == id && p.Language == language);
            var image = "";
            switch (type)
            {
                case (int)DeviceType.PC:
                    image = data.Image;
                    data.Image = string.Empty;
                    break;
                case (int)DeviceType.PCHover:
                    image = data.HoverImage;
                    data.HoverImage = string.Empty;
                    break;
            }
            db.SaveChanges();

            return image;
        }

        public string DeleteSliderImage(int id)
        {
            TopicImg data = db.TopicImgs.Find(id);
            var image = data.Image;
            data.Image = string.Empty;       
            db.SaveChanges();

            return image;
        }
        public List<string> DeleteHoverImages(int id)
        {
            var images = db.TopicTranslations.Where(p => p.TopicId == id).Select(p => p.HoverImage).ToList();
            return images;
        }

        public List<string> DeleteImages(int id)
        {
            var images = db.TopicTranslations.Where(p => p.TopicId == id).Select(p => p.Image).ToList();
            return images;
        }

        public void Delete(int id)
        {
            Topic topic = db.Topic.Find(id);
            db.Topic.Remove(topic);

            IEnumerable<TopicTranslations> topicTranslations = db.TopicTranslations.Where(p => p.TopicId == id);
            db.TopicTranslations.RemoveRange(topicTranslations);

            foreach (var item in topicTranslations)
            {
                IEnumerable<TopicInformation> topicInformation = db.TopicInformations.Where(p => p.TopicTranslationsId == item.ID);
                foreach (var topicInformations in topicInformation)
                {
                    IEnumerable<TopicImg> topicImg = db.TopicImgs.Where(p => p.TopicInformationId == topicInformations.ID);
                    db.TopicImgs.RemoveRange(topicImg);
                }
                db.TopicInformations.RemoveRange(topicInformation);


            }


            db.SaveChanges();
        }
        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RelTopicTopicTranslations Detail(int topicId, Language language, string category)
        {

            var joinQuery = from n in db.Topic
                        join t in db.TopicTranslations on n.ID equals t.TopicId
                        select new RelTopicTopicTranslations()
                        {
                            ID = n.ID,
                            Image = n.Image,
                            HoverImage = n.HoverImage,
                            OnlineTime = n.OnlineTime,
                            IsLimit = n.IsLimit,
                            Status = n.Status,
                            ProductId = n.ProductId,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            Title = t.Title,
                            TranslationsHoverImage = t.HoverImage,
                            LinkUrl = t.LinkUrl,
                            TranslationsImage = t.Image,
                            Language = t.Language,
                            Tags = t.Tags,
                            TopicInformations = t.TopicInformations.Where(p =>p.Status == true && p.TopicTranslationsId == t.ID).OrderBy(p => p.Sort).ToList(),
                            TopicTranslationsID = t.ID,
                        };
            joinQuery = joinQuery.Where(p => p.Language == (int)language && p.Status == true && p.ID == topicId);
            if (category == "women") {
                joinQuery = joinQuery.Where(p => p.IsLimit == false);
            }
            if (joinQuery.Count() != 0) {
                var query = joinQuery.Single();
                return query;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public IQueryable<RelTopicTopicTranslations> List(int gameId ,Language language, string category)
        {
            var query = Query(null, gameId, true, (int)language);
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime dtCurrentTime = TimeZoneInfo.ConvertTime(DateTime.Now, tzi);
            query = query.Where(p => p.OnlineTime <= dtCurrentTime).OrderByDescending(p => p.OnlineTime);
            //是否為限制級
            if (category =="women") {
                query = query.Where(p => p.IsLimit == false);
            }
            return query.OrderByDescending(p => p.OnlineTime);
        }
        /// <summary>
        /// 前台顯示用列表
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public RelTopicTopicTranslations Preview(int gameId, Language language)
        {

            var joinQuery = from n in db.Topic
                            join t in db.TopicTranslations on n.ID equals t.TopicId
                            select new RelTopicTopicTranslations()
                            {
                                ID = n.ID,
                                Image = n.Image,
                                HoverImage = n.HoverImage,
                                OnlineTime = n.OnlineTime,
                                IsLimit = n.IsLimit,
                                ProductId = n.ProductId,
                                CreateTime = n.CreateTime,
                                Creater = n.Creater,
                                UpdateTime = n.UpdateTime,
                                Updater = n.Updater,
                                Title = t.Title,
                                TranslationsHoverImage = t.HoverImage,
                                TranslationsImage = t.Image,
                                Language = t.Language,
                                Tags = t.Tags,
                                TopicInformations = t.TopicInformations.Where(p => p.Status == true).OrderBy(p => p.Sort).ToList(),
                                TopicTranslationsID = t.ID,
                            };
            joinQuery = joinQuery.Where(p => p.Language == (int)language);
            if (joinQuery.Any())
            {
                var query = joinQuery.Single(p => p.ID == gameId && p.Language == (int)language);
                return query;
            }
            return null;
        }
        #endregion
    }

}
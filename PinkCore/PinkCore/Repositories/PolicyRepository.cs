﻿using AutoMapper;
using PinkCore.Areas.Admin.ViewModels.Policy;
using PinkCore.Models;
using PinkCore.Models.Cmind;
using PinkCore.Models.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Repositories
{
    public class PolicyRepository
    {
        private PinkCoreDBEntities db = new PinkCoreDBEntities();
        #region 後台        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Policy"></param>
        /// <param name="PolicyTranslationsTw">
        /// 繁中
        /// </param>
        /// <param name="PolicyTranslationsCn">
        /// 簡中
        /// </param>
        /// <param name="PolicyTranslationsUs">
        /// 英文
        /// </param>
        /// <param name="PolicyTranslationsJp">
        /// 日文
        /// </param>
        /// <param name="PolicyTranslationsKr">
        /// 韓文
        /// </param>
        public int Insert(Policy Policy, List<PolicyTranslations> PolicyTranslations)
        {
            DateTime now = DateTime.Now;
            Policy.CreateTime = now;
            Policy.UpdateTime = now;
            db.Policy.Add(Policy);
            db.SaveChanges();

            int PolicyId = Policy.ID;

            foreach (var item in PolicyTranslations)
            {
                item.PolicyId = PolicyId;
                db.PolicyTranslations.Add(item);
            }
            db.SaveChanges();

            return PolicyId;
        }

        public void Update(Policy newData, List<PolicyTranslations> list)
        {
            DateTime now = DateTime.Now;

            Policy oldData = db.Policy.Find(newData.ID);
            oldData.UpdateTime = now;

            foreach (var item in list)
            {
                PolicyTranslations dataTranslations = db.PolicyTranslations.Find(item.ID);
                dataTranslations.PrivacyContent = item.PrivacyContent;
                dataTranslations.PolicyContent = item.PolicyContent;
            }

            db.SaveChanges();
        }

        public IQueryable<RelPolicyPolicyTranslations> GetAll()
        {
            var query = from n in db.Policy
                        join t in db.PolicyTranslations on n.ID equals t.PolicyId
                        select new RelPolicyPolicyTranslations()
                        {
                            ID = n.ID,
                            CreateTime = n.CreateTime,
                            Creater = n.Creater,
                            UpdateTime = n.UpdateTime,
                            Updater = n.Updater,
                            PolicyTranslationsID = t.ID,
                            Language = t.Language,
                            PrivacyContent = t.PrivacyContent,
                            PolicyContent = t.PolicyContent
                        };
            return query;
        }

        public PolicyView FindById(int id)
        {
            Policy data = db.Policy.Find(id);
            
            var result = Mapper.Map<PolicyView>(data);
            if (data != null) {
                var PolicyTranslationViewTw = db.PolicyTranslations.Single(p => p.PolicyId == id && p.Language == (int)Language.zh_TW);
                result.PolicyTranslationViewTw = Mapper.Map<PolicyTranslationView>(PolicyTranslationViewTw);

                var PolicyTranslationViewCn = db.PolicyTranslations.Single(p => p.PolicyId == id && p.Language == (int)Language.zh_CN);
                result.PolicyTranslationViewCn = Mapper.Map<PolicyTranslationView>(PolicyTranslationViewCn);

                var PolicyTranslationViewUs = db.PolicyTranslations.Single(p => p.PolicyId == id && p.Language == (int)Language.en_US);
                result.PolicyTranslationViewUs = Mapper.Map<PolicyTranslationView>(PolicyTranslationViewUs);

                var PolicyTranslationViewJp = db.PolicyTranslations.Single(p => p.PolicyId == id && p.Language == (int)Language.ja_JP);
                result.PolicyTranslationViewJp = Mapper.Map<PolicyTranslationView>(PolicyTranslationViewJp);

                var PolicyTranslationViewKr = db.PolicyTranslations.Single(p => p.PolicyId == id && p.Language == (int)Language.ko_KR);
                result.PolicyTranslationViewKr = Mapper.Map<PolicyTranslationView>(PolicyTranslationViewKr);
            }

            return result;
        }

        public void Delete(int id)
        {
            Policy Policy = db.Policy.Find(id);
            db.Policy.Remove(Policy);

            IEnumerable<PolicyTranslations> PolicyTranslations = db.PolicyTranslations.Where(p => p.PolicyId == id);
            db.PolicyTranslations.RemoveRange(PolicyTranslations);

            db.SaveChanges();
        }

        #endregion

        #region 前台        
        /// <summary>
        /// 前台顯示用內容頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RelPolicyPolicyTranslations Detail(int id, Language language)
        {
            var list = GetAll().Where(p => p.Language == (int)language && p.ID == id);
            var query = list.Single();
            return query;
        }
        #endregion
    }
}
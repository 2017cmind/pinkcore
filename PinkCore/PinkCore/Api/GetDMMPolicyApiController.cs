﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net;
using PinkCore.ViewModels;
using PinkCore.Repositories;
using PinkCore.Models.Cmind;
using System.Web.Http;
using System.Web.Http.Cors;
namespace PinkCore.Api
{
    public class GetDMMPolicyApiController : System.Web.Http.ApiController
    {
        private PolicyRepository policyRepository = new PolicyRepository();
        /// <summary>
        /// 機台首頁 UI 顯示
        /// 不用驗證、不用接收參數
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/getDmmPolicy")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage GetDMMPolicy(string lang)
        {
            
            CommonResponse<Models.Api.GetDMMPolicyApiResult> response = new CommonResponse<Models.Api.GetDMMPolicyApiResult>();
            try
            {
                var policy = policyRepository.FindById(2);
                var privacyPolicyContent = "";
                var serviceTermsContent = "";

                switch (lang)
                {
                    case "zh_TW":
                        privacyPolicyContent = policy.PolicyTranslationViewTw.PrivacyContent;
                        serviceTermsContent = policy.PolicyTranslationViewTw.PolicyContent;
                        break;
                    case "zh_CN":
                        privacyPolicyContent = policy.PolicyTranslationViewCn.PrivacyContent;
                        serviceTermsContent = policy.PolicyTranslationViewCn.PolicyContent;
                        break;
                    case "en_US":
                        privacyPolicyContent = policy.PolicyTranslationViewUs.PrivacyContent;
                        serviceTermsContent = policy.PolicyTranslationViewUs.PolicyContent;
                        break;
                    case "ja_JP":
                        privacyPolicyContent = policy.PolicyTranslationViewJp.PrivacyContent;
                        serviceTermsContent = policy.PolicyTranslationViewJp.PolicyContent;
                        break;
                    case "ko_KR":
                        privacyPolicyContent = policy.PolicyTranslationViewKr.PrivacyContent;
                        serviceTermsContent = policy.PolicyTranslationViewTw.PolicyContent;
                        break;
                    default:
                        break;
                }
                Models.Api.GetDMMPolicyApiResult result = new Models.Api.GetDMMPolicyApiResult()
                {
                    PrivacyPolicy = privacyPolicyContent,
                    ServiceTerms = serviceTermsContent,
                };
                response.Result = result;
                response.Success();
            }
            catch (Exception ex)
            {
                response.SetError(ErrorCode.ReturnDataError);
            }
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
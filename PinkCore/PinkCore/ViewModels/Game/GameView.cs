﻿using PinkCore.ViewModels.Base;
using System;
using System.Collections.Generic;
using PinkCore.Models.Cmind;
using System.Linq;
using System.Web;

namespace PinkCore.ViewModels.Game
{
    public class GameView : BaseView
    {
        public IEnumerable<GameItem> DataList { get; set; }
        public bool IsMoblie { get; set; }
    }
    public class GameItem
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public string Image { get; set; }
        public string HoverImage { get; set; }
        public int Sort { get; set; }
        public System.DateTime OnlineTime { get; set; }
        public bool Status { get; set; }
        public string SupportLan { get; set; }
        public bool IsTwLan
        {
            get
            {
                var value = false;
                if (!string.IsNullOrEmpty(SupportLan))
                {
                    value = SupportLan.IndexOf(((int)Models.Cmind.Language.zh_TW).ToString()) != -1 ? true : false;
                }
                return value;
            }
        }
        public bool IsCnLan
        {
            get
            {
                var value = false;
                if (!string.IsNullOrEmpty(SupportLan))
                {
                    value = SupportLan.IndexOf(((int)Models.Cmind.Language.zh_CN).ToString()) != -1 ? true : false;
                }
                return value;
            }
        }
        public bool IsEnLan
        {
            get
            {
                var value = false;
                if (!string.IsNullOrEmpty(SupportLan))
                {
                    value = SupportLan.IndexOf(((int)Models.Cmind.Language.en_US).ToString()) != -1 ? true : false;
                }
                return value;
            }
        }
        public bool IsJpLan
        {
            get
            {
                var value = false;
                if (!string.IsNullOrEmpty(SupportLan))
                {
                    value = SupportLan.IndexOf(((int)Models.Cmind.Language.ja_JP).ToString()) != -1 ? true : false;
                }
                return value;
            }
        }
        public bool IsKRLan
        {
            get
            {
                var value = false;
                if (!string.IsNullOrEmpty(SupportLan))
                {
                    value = SupportLan.IndexOf(((int)Models.Cmind.Language.ko_KR).ToString()) != -1 ? true : false;
                }
                return value;
            }
        }
        public bool isHot { get; set; }
        public bool IsLimit { get; set; }

        public int Updater { get; set; }
        public int Language { get; set; }
        public int ProductTranslationsID { get; set; }
        public string TranslationsHoverImage { get; set; }
        public string TranslationsImage { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
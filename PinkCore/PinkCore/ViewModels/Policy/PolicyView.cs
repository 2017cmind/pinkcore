﻿using PinkCore.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.ViewModels.Policy
{
    public class PolicyView : BaseView
    {
        public int ID { get; set; }
        public int Language { get; set; }
        public int PolicyTranslationsID { get; set; }
        public string PrivacyContent { get; set; }
        public string PolicyContent { get; set; }
    }
}
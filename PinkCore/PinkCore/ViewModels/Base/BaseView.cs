﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PinkCore.Models.Cmind;

namespace PinkCore.ViewModels.Base
{
    public class BaseView 
    {
        public Language Language { get; set; }
        public string AgeMoblieImage { get; set; }
        public string AgePCImage { get; set; }
        public bool IsCookie { get; set; }
    }
}
﻿using PinkCore.Models.Cmind;
using PinkCore.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PinkCore.ViewModels.About
{
    public class AboutView : BaseView
    {
        public int ID { get; set; }
        public string AboutImg { get; set; }
        public string ContactImg { get; set; }
        public string BannerImg { get; set; }
        public int AboutTranslationsID { get; set; }
        public string ContactTitle { get; set; }
        public string AboutContent { get; set; }
        public int Language { get; set; }
        public string ContactContent { get; set; }
        public string AboutTitle { get; set; }
        public List<AboutImgView> AboutImgs { get; set; }
    }
    public class AboutImgView
    {
        public Language Language { get; set; }

        public int ID { get; set; }
        public int AboutTranslationsId { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "敘述")]
        public string Description { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }
    }
}
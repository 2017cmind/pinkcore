﻿using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using PinkCore.ViewModels.Base;
using PinkCore.ViewModels.Game;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PinkCore.ViewModels.Topic 
{
    public class TopicView : BaseView
    {
        public IEnumerable<TopicItem> DataList { get; set; }
        public IEnumerable<TopicItem> AllDataList { get; set; }
    }
    public class TopicItem : BaseView
    {
        ProductRepository productRepository = new ProductRepository();

        public int ID { get; set; }
        public int ProductId { get; set; }
        public bool IsMoblie { get; set; }
        public string ProductName
        {
            get
            {
                var result = "";
                switch (this.Language)
                {
                    case ((int)Models.Cmind.Language.zh_TW):
                        result = productRepository.FindBy(ProductId).ProductTranslationViewTw.Name;
                        break;
                    case ((int)Models.Cmind.Language.zh_CN):
                        result = productRepository.FindBy(ProductId).ProductTranslationViewCn.Name;
                        break;
                    case ((int)Models.Cmind.Language.en_US):
                        result = productRepository.FindBy(ProductId).ProductTranslationViewUs.Name;
                        break;
                    case ((int)Models.Cmind.Language.ja_JP):
                        result = productRepository.FindBy(ProductId).ProductTranslationViewJp.Name;
                        break;
                    case ((int)Models.Cmind.Language.ko_KR):
                        result = productRepository.FindBy(ProductId).ProductTranslationViewKr.Name;
                        break;
                }
                return result;
            }
        }
        public string Image { get; set; }
        public string HoverImage { get; set; }
        public System.DateTime OnlineTime { get; set; }
        public bool IsLimit { get; set; }
        public bool Status { get; set; }
        public int TopicTranslationsID { get; set; }
        public string Title { get; set; }
        public string LinkUrl { get; set; }
        public string TranslationsImage { get; set; }
        public string TranslationsHoverImage { get; set; }
        public int Language { get; set; }
        public string Tags { get; set; }
        public List<TopicInformationsView> TopicInformations { get; set; }
        public IEnumerable<TopicInformationsView> TopTopicInformations { get; set; }
        public IEnumerable<TopicInformationsView> OtherTopicInformations { get; set; }
        public GameItem GameData { get; set; }

        public IEnumerable<TopicItem> DataList { get; set; }


    }
    public class TopicInformationsView
    {
        public int ID { get; set; }

        public Guid ItemIndex { get; set; }

        public int TopicTranslationsId { get; set; }

        [Display(Name = "排版類型")]
        public ArrangementType Arrangement { get; set; }

        [Display(Name = "影片連結")]
        public string VideoUrl { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "內容")]
        public string Content { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        public List<TopicImgView> TopicImgs { get; set; }
    }

    public class TopicImgView
    {
        public int ID { get; set; }

        public int TopicInformationId { get; set; }

        [Display(Name = "語言")]
        public int Language { get; set; }

        [Display(Name = "圖片")]
        public string Image { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }
    }
}
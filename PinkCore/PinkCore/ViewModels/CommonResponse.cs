﻿using Newtonsoft.Json;
using PinkCore.Models.Cmind;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.ViewModels
{
    /// <summary>
    /// 共通Response
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommonResponse<T>
    {
        public CommonResponse()
        {
            Result = default(T);
        }

        [JsonProperty(PropertyName = "isSuccess")]
        public bool IsSuccess
        {
            get
            {
                return this.ErrorMessage.Code == ErrorCode.Success;
            }
        }

        [JsonProperty(PropertyName = "result")]
        public T Result { get; set; }

        private ErrorMessage errorMessage = new ErrorMessage();

        [JsonProperty(PropertyName = "error_msg")]
        public ErrorMessage ErrorMessage
        {
            get { return this.errorMessage; }
        }

        /// <summary>
        /// 成功時的處裡方式
        /// </summary>
        public void Success()
        {
            this.errorMessage.Code = ErrorCode.Success;
            this.errorMessage.Message = EnumHelper.GetDescription(errorMessage.Code);
        }

        /// <summary>
        /// 設定錯誤訊息
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="message"></param>
        public void SetError(ErrorCode errorCode, string message = "")
        {
            this.errorMessage.Code = errorCode;
            if (!string.IsNullOrEmpty(message))
            {
                this.errorMessage.Message = message;
            }
            else
            {
                this.errorMessage.Message = EnumHelper.GetDescription(errorCode);
            }
        }
    }

    public class ErrorMessage
    {
        public ErrorMessage()
        {
            Code = ErrorCode.ReturnDataError;
            Message = EnumHelper.GetDescription(Code);
        }

        [JsonProperty(PropertyName = "code")]
        public ErrorCode Code { get; set; }

        [JsonProperty(PropertyName = "msg")]
        public string Message { get; set; }
    }
}
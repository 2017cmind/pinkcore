﻿using PinkCore.ViewModels.Base;
using PinkCore.ViewModels.Game;
using PinkCore.ViewModels.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.ViewModels.Home
{
    public class HomeView : BaseView
    {
        public IEnumerable<BannerItem> BannerList { get; set; }
        public IEnumerable<GameItem> GameList { get; set; }
        public IEnumerable<TopicItem> TopicList { get; set; }
        public IEnumerable<TopicItem> AllTopicList { get; set; }
        public bool IsMoblie { get; set; }
    }
    public class BannerItem
    {
        public int ID { get; set; }
        public string Icon { get; set; }
        public string PCImage { get; set; }
        public string MoblieImage { get; set; }
        public string HoverImage { get; set; }
        public bool isLimit { get; set; }
        public bool Status { get; set; }
        public int Sort { get; set; }

        public int BannerTranslationsID { get; set; }
        public string TranslationsPCImage { get; set; }
        public string TranslationsMoblieImage { get; set; }
        public string TranslationsHoverImage { get; set; }
        public int Language { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
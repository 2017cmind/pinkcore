﻿using PinkCore.Models.Cmind;
using PinkCore.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.ActionFilters
{
    public class CultureFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string lang = filterContext.RouteData.Values["lang"].ToString();
            Language langCulture = LanguageHelper.GetLanguage(lang);
            string culture = langCulture.ToString().Replace("_", "-");

            //設定多語系
            CultureInfo ci = new CultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(ci.Name);
            base.OnActionExecuting(filterContext);
        }
    }
}
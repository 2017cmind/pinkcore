﻿using PinkCore.Controllers;
using PinkCore.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.ActionFilters
{
    public class PreviewAuthorizeActionFilters : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool login = AdminInfoHelper.GetAdminInfo() == null ? false : true;
            if (!login)
            {
                BaseController baseController = new BaseController();
                filterContext.Result = baseController.Error();
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
﻿using PinkCore.Controllers;
using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using PinkCore.Utility;
using PinkCore.Utility.Cmind;
using PinkCore.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.ActionFilters
{
    public class LayoutActionFilter : ActionFilterAttribute
    {
        private AgeConfirmRepository ageConfirmRepository = new AgeConfirmRepository();
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Result is ViewResultBase)//Gets ViewResult and PartialViewResult
            {

                object viewModel = ((ViewResultBase)filterContext.Result).Model;

                if (viewModel != null && viewModel is BaseView)
                {
                    BaseView model = viewModel as BaseView;
                    string lang = HttpContext.Current.Request.RequestContext.RouteData.Values["lang"].ToString();

                    Language langCulture = LanguageHelper.GetLanguage(lang);
                    var query = ageConfirmRepository.Detail(langCulture);
                    if (query != null)
                    {
                        model.Language = langCulture;
                        model.AgeMoblieImage = !string.IsNullOrEmpty(query.TranslationsMoblieImage) ? query.TranslationsMoblieImage : query.MoblieImage;
                        model.AgePCImage = !string.IsNullOrEmpty(query.TranslationsPCImage) ? query.TranslationsPCImage : query.PCImage;
                    }
                    model.IsCookie = CookieHelper.Get("rayark_adult_prohibitions") != null ? true : false;
                }
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
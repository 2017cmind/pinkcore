﻿using PinkCore.Models.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Utility
{
    public class LanguageHelper
    {
        /// <summary>
        /// 語系預設值
        /// </summary>
        private static Language defaultLanguage = Language.zh_TW;

        public static Language GetLanguage(string language)
        {
            language = language.Replace("-", "_");
            try
            {
                var langCulture = (Language)Enum.Parse(typeof(Language), language, false);
                return langCulture;
            }
            catch (Exception ex)
            {
                //預設值
                return defaultLanguage;
            }
        }
    }
}
﻿using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Repositories;
using PinkCore.Utility.Cmind;
using PinkCore.ViewModels.Game;
using PinkCore.ViewModels.Home;
using PinkCore.ViewModels.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Controllers
{
    [LayoutActionFilter]
    [ErrorHandleActionFilter]
    public class HomeController : BaseController
    {
        private BannerRepository bannerRepository = new BannerRepository();
        private ProductRepository productRepository = new ProductRepository();
        private TopicRepository topicRepository = new TopicRepository();

        public ActionResult Index()
        {
            var language = GetLanguage();
            HomeView model = new HomeView();
            var category = GetCategory();
            var bannerQuery = bannerRepository.List(language, category);
            model.BannerList = Mapper.Map<IEnumerable<BannerItem>>(bannerQuery);
            var gameQuery = productRepository.List(language, category).Where(p => p.isHot == true).Take(3);
            model.GameList = Mapper.Map<IEnumerable<GameItem>>(gameQuery);
            var topicQuery = topicRepository.List(0, language, category).Take(5);
            model.TopicList = Mapper.Map<IEnumerable<TopicItem>>(topicQuery);
            var allTopicQuery = topicRepository.List(0, language, category);
            model.AllTopicList = Mapper.Map<IEnumerable<TopicItem>>(allTopicQuery);
            model.IsMoblie = IsFromMobile();
            return View(model);
        }
    }
}
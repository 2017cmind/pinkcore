﻿using AutoMapper;
using PinkCore.ViewModels.Policy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PinkCore.Repositories;
using PinkCore.ActionFilters;

namespace PinkCore.Controllers
{
    [LayoutActionFilter]
    [ErrorHandleActionFilter]
    public class PolicyController : BaseController
    {
        private PolicyRepository policyRepository = new PolicyRepository();
        // GET: Policy
        public ActionResult Index()
        {
            var language = GetLanguage();
            PolicyView model = new PolicyView();
            //id = 1是pinkcore的隱私政策
            var query = policyRepository.Detail(1,language);
            model = Mapper.Map<PolicyView>(query);
            model.IsCookie = GetCategory() != null ? true : false;
            return View(model);
        }
    }
}
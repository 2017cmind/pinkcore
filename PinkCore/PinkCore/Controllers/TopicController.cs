﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Models.Cmind;
using PinkCore.Repositories;
using PinkCore.ViewModels.Game;
using PinkCore.ViewModels.Topic;

namespace PinkCore.Controllers
{
    [LayoutActionFilter]
    [ErrorHandleActionFilter]
    public class TopicController : BaseController
    {
        private TopicRepository topicRepository = new TopicRepository();
        private ProductRepository productRepository = new ProductRepository();

        // GET: Topic
        public ActionResult Index(int game = 0)
        {
            var language = GetLanguage();
            var category = GetCategory();
          
            TopicView model = new TopicView();
            var topicQuery = topicRepository.List(game,language, category);
            var allTopicQuery = topicRepository.List(0, language, category);
            model.DataList = Mapper.Map<IEnumerable<TopicItem>>(topicQuery);
            model.AllDataList = Mapper.Map<IEnumerable<TopicItem>>(allTopicQuery);
            model.IsCookie = GetCategory() != null ? true: false ;
            return View(model);
        }
        public ActionResult Detail(string id)
        {
            var language = GetLanguage();
            var category = GetCategory();
            var model = new TopicItem();

            if (string.IsNullOrEmpty(id) || id == "Index")
            {
                return RedirectToAction("Detail", new { lang = HttpContext.Request.RequestContext.RouteData.Values["lang"].ToString() });
            }

            //string name = redirectToFriendUrl(id);
            //if (!string.IsNullOrEmpty(name))
            //{
            //    return RedirectToAction("Detail", new { id = name });
            //}
            int numId = 0;
            if (!string.IsNullOrEmpty(id) && !int.TryParse(id, out numId))
            {
                var topicdata = topicRepository.FindIdByLinkTitle(id, (int)language);
                if (topicdata == null)
                {
                    topicdata = topicRepository.FindTopicIdByLinkTitle(id);
                    var topicId = topicdata.TopicId;
                    var query = topicRepository.Detail(topicId, language, category);

                    var name =  !string.IsNullOrEmpty(query.LinkUrl) ? query.LinkUrl : topicId.ToString();

                    return RedirectToAction("Detail", new { id = name });
                }
                else {
                    var topicId = topicdata.TopicId;
                    var query = topicRepository.Detail(topicId, language, category);
                    query.TopicInformations = topicdata.TopicInformations.ToList();
                    if (query != null)
                    {
                        model = Mapper.Map<TopicItem>(query);
                        model.IsMoblie = IsFromMobile();
                        var gameQuery = productRepository.Detail(model.ProductId, language, category);
                        model.GameData = Mapper.Map<GameItem>(gameQuery);
                        var topicQuery = topicRepository.List(0, language, category);
                        model.DataList = Mapper.Map<IEnumerable<TopicItem>>(topicQuery);
                    }
                    else
                    {
                        return RedirectToAction("Error", new { lang = HttpContext.Request.RequestContext.RouteData.Values["lang"].ToString() });
                    }
                }
               
            }
            if(int.TryParse(id, out numId)){
                var query = topicRepository.Detail(numId, language, category);
                if (query != null)
                {
                    if (!string.IsNullOrEmpty(query.LinkUrl))
                    {
                        return RedirectToAction("Detail", new { id = query.LinkUrl });
                    }
                    model = Mapper.Map<TopicItem>(query);
                    model.IsMoblie = IsFromMobile();
                    var gameQuery = productRepository.Detail(model.ProductId, language, category);
                    model.GameData = Mapper.Map<GameItem>(gameQuery);
                    var topicQuery = topicRepository.List(0, language, category);
                    model.DataList = Mapper.Map<IEnumerable<TopicItem>>(topicQuery);
                }
                else
                {
                    return RedirectToAction("Error", new { lang = HttpContext.Request.RequestContext.RouteData.Values["lang"].ToString() });
                }
            }
            return View(model);
        }

        private string redirectToFriendUrl(string id)
        {
            var language = GetLanguage();
            var category = GetCategory();
            string name = string.Empty;
            int numId = 0;
            if (int.TryParse(id, out numId))
            {
                var data = topicRepository.Detail(numId, language,category);
                if (data != null)
                {
                    if (!string.IsNullOrEmpty(data.LinkUrl))
                    {
                        name = data.LinkUrl;
                    }
                    else
                    {
                        var twdata = topicRepository.Detail(numId, language, category);
                        name = Regex.Replace(twdata.LinkUrl, @"\s", "-");
                    }
                }

            }
          
            return name;
        }

        [PreviewAuthorizeActionFilters]
        public ActionResult Preview(int id)
        {
            var language = GetLanguage();
            var query = topicRepository.Preview(id, language);

            if (query == null)
            {
                return RedirectToAction("NotFound");
            }
            var model = new TopicItem();
            model = Mapper.Map<TopicItem>(query);
            model.IsMoblie = IsFromMobile();
            var gameQuery = productRepository.Preview(model.ProductId, language);
            model.GameData = Mapper.Map<GameItem>(gameQuery);
            var gameId = model.GameData != null ? model.GameData.ID : 0;
            var topicQuery = topicRepository.Query(null ,gameId, true, (int)language);
            model.DataList = Mapper.Map<IEnumerable<TopicItem>>(topicQuery);
            return View("Detail", model);
        }
    }
}
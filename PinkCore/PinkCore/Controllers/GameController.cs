﻿using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Repositories;
using PinkCore.ViewModels.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Controllers
{
    [LayoutActionFilter]
    [ErrorHandleActionFilter]
    public class GameController : BaseController
    {
        private ProductRepository productRepository = new ProductRepository();

        // GET: Game
        public ActionResult Index()
        {
            var category = GetCategory();
            var language = GetLanguage();
            GameView model = new GameView();
            var gameQuery = productRepository.List(language, category);
            model.DataList = Mapper.Map<IEnumerable<GameItem>>(gameQuery);
            model.IsCookie = GetCategory() != null ? true : false;
            model.IsMoblie = IsFromMobile();
            return View(model);
        }
    }
}
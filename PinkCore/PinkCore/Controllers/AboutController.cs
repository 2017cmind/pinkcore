﻿using AutoMapper;
using PinkCore.ActionFilters;
using PinkCore.Repositories;
using PinkCore.ViewModels.About;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PinkCore.Controllers
{
    [LayoutActionFilter]
    [ErrorHandleActionFilter]
    public class AboutController : BaseController
    {
        private AboutRepository aboutRepository = new AboutRepository();

        // GET: About
        public ActionResult Index()
        {
            var language = GetLanguage();
            AboutView model = new AboutView();
            var query = aboutRepository.Detail(1, language);
            model = Mapper.Map<AboutView>(query);
            model.IsCookie = GetCategory() != null ? true : false;

            return View(model);
        }
    }
}
﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.Api
{
    public class GetDMMPolicyApiResult
    {
        /// <summary>
        /// 隱私條款
        /// </summary>
        [JsonProperty(PropertyName = "privacy_policy")]
        public string PrivacyPolicy { get; set; }

        /// <summary>
        /// 服務條款
        /// </summary>
        [JsonProperty(PropertyName = "service_terms")]
        public string ServiceTerms { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.Cmind
{
    public class AdminInfo
    {
        public int ID { get; set; }

        public string Account { get; set; }

        public bool isAdmin { get; set; }
    }
}
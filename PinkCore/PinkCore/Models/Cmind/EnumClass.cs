﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PinkCore.Models.Cmind
{
    /// <summary>
    /// 裝置類別
    /// </summary>
    public enum DeviceType
    {
        /// <summary>
        /// 電腦版
        /// </summary>
        [Description("電腦版")]
        PC = 1,

        /// <summary>
        /// 手機版        
        /// </summary>
        [Description("手機版")]
        Moblie = 2,

        /// <summary>
        /// 電腦替換圖        
        /// </summary>
        [Description("電腦替換圖")]
        PCHover = 3,
    }

    /// <summary>
    /// 排版類別
    /// </summary>
    public enum ArrangementType
    {
        /// <summary>
        /// 圖文
        /// </summary>
        [Description("圖文")]
        Article = 1,

        /// <summary>
        /// 輪播        
        /// </summary>
        [Description("輪播")]
        Silders = 2
    }

    /// <summary>
    /// 關於我們圖片類別
    /// </summary>
    public enum AboutImgType
    {
        /// <summary>
        /// 關於我們圖片
        /// </summary>
        [Description("關於我們圖片")]
        AboutImg = 2,
        /// <summary>
        /// 聯絡我們主圖
        /// </summary>
        [Description("聯絡我們圖片")]
        ContactImg = 3,
        /// <summary>
        /// 關於我們Banner圖片
        /// </summary>
        [Description("Banner")]
        BannerImg = 1
    }

    public enum ProductType
    {
        /// <summary>
        /// 無
        /// </summary>
        [Description("無")]
        None = 1,
        /// <summary>
        /// 人氣遊戲
        /// </summary>
        [Description("人氣遊戲")]
        HotGame = 2,

        /// <summary>
        /// 即將推出
        /// </summary>
        [Description("即將推出")]
        NewGame = 3,

        /// <summary>
        /// 必玩推薦
        /// </summary>
        [Description("必玩推薦")]
        Recommend = 4,

    }


    /// <summary>
    /// 語系
    /// </summary>
    public enum Language
    {
        /// <summary>
        /// 繁體中文
        /// </summary>
        [Description("繁體")]
        zh_TW = 1,

        /// <summary>
        /// 簡體中文
        /// </summary>
        [Description("簡體")]
        zh_CN = 2,

        /// <summary>
        /// 英文
        /// </summary>
        [Description("英文")]
        en_US = 3,

        /// <summary>
        /// 日文
        /// </summary>
        [Description("日文")]
        ja_JP = 4,

        /// <summary>
        /// 韓文
        /// </summary>
        [Description("韓文")]
        ko_KR = 5,
    }
    public enum ErrorCode
    {
        /// <summary>
        /// 傳送正確
        /// </summary>
        [Description("傳送正確")]
        Success = 0,

        /// <summary>
        /// 查無資料
        /// </summary>
        [Description("查無資料")]
        NoData = 1001,

        /// <summary>
        /// 傳入參數(解析)錯誤
        /// </summary>
        [Description("傳入參數(解析)錯誤")]
        ParameterError = 1002,

        /// <summary>
        /// 回傳資料解析錯誤
        /// </summary>
        [Description("回傳資料解析錯誤")]
        ReturnDataError = 1003,
    }
}
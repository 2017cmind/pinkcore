﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.Cmind
{
    public class SiteSetting
    {
        /// <summary>
        /// 網站名稱
        /// </summary>
        public const string SiteName = "PinkCore";

        /// <summary>
        /// 網站名稱縮寫
        /// </summary>
        public const string SiteNameAbbreviation = "PC";

        /// <summary>
        /// 維護商
        /// </summary>
        public const string Maintainer = "Cmind 思脈數位";

        /// <summary>
        /// 維護商網站
        /// </summary>
        public const string MaintainerWebSite = "http://www.cmind.com.tw/";
    }
}
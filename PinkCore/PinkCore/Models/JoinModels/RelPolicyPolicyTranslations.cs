﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.JoinModels
{
    public class RelPolicyPolicyTranslations
    {
        public int ID { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }
        public int Language { get; set; }
        public int PolicyTranslationsID { get; set; }
        public string PrivacyContent { get; set; }
        public string PolicyContent { get; set; }

    }
}
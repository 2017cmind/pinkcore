﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.JoinModels
{
    public class RelBannerBannerTranslations
    {
        public int ID { get; set; }
        public string Icon { get; set; }
        public string PCImage { get; set; }
        public string MoblieImage { get; set; }
        public string HoverImage { get; set; }
        public bool isLimit { get; set; }
        public System.DateTime CreateTime { get; set; }
        public bool Status { get; set; }
        public int Sort { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }

        public int BannerTranslationsID { get; set; }
        public string TranslationsPCImage { get; set; }
        public string TranslationsMoblieImage { get; set; }
        public string TranslationsHoverImage { get; set; }
        public int Language { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.JoinModels
{
    public class RelAgeConfirmAgeConfirmTranslations
    {
        public int ID { get; set; }
        public string MoblieImage { get; set; }
        public string PCImage { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }
        public int Language { get; set; }
        public int AgeConfirmTranslationsID { get; set; }
        public string TranslationsMoblieImage { get; set; }
        public string TranslationsPCImage { get; set; }
    }
}
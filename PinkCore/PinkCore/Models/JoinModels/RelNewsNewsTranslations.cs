﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.JoinModels
{
    public class RelNewsNewsTranslations
    {
        public int ID { get; set; }
        public System.DateTime OnlineTime { get; set; }
        public int ProductId { get; set; }
        public string Image { get; set; }
        public bool Status { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }

        public int NewsTranslationsID { get; set; }
        public string TranslationsImage { get; set; }
        public int Language { get; set; }
        public string Title { get; set; }
        public string LinkUrl { get; set; }
        public string Tags { get; set; }
        public string Content { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.JoinModels
{
    public class RelProductProductTranslations
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public string Image { get; set; }
        public string HoverImage { get; set; }
        public int Sort { get; set; }
        public System.DateTime OnlineTime { get; set; }
        public bool Status { get; set; }
        public string SupportLan { get; set; }
        public bool isHot { get; set; }
        public bool IsLimit { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }
        public int Language { get; set; }
        public int ProductTranslationsID { get; set; }
        public string TranslationsHoverImage { get; set; }
        public string TranslationsImage { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinkCore.Models.JoinModels
{
    public class RelAboutAboutTranslations
    {
        public int ID { get; set; }
        public string AboutImg { get; set; }
        public string ContactImg { get; set; }
        public string BannerImg { get; set; }
        public System.DateTime CreateTime { get; set; }
        public int Creater { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public int Updater { get; set; }

        public int AboutTranslationsID { get; set; }
        public string ContactTitle { get; set; }
        public string AboutContent { get; set; }
        public int Language { get; set; }
        public string ContactContent { get; set; }
        public string AboutTitle { get; set; }
        public List<AboutImg> AboutImgs { get; set; }
    }
}